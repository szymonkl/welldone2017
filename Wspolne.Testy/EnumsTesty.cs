﻿using NUnit.Framework;

namespace Wspolne.Testy
{
    [TestFixture]
    public class EnumsTesty
    {
        [Test]
        public void EnumsTesty_ListaJednostekDlugosci_PoprawnaIlosc()
        {
            Assert.AreEqual(5, Enums.JednostkiDlugosci.Count);
        }

        [Test]
        public void EnumsTesty_ListaJednostekPredkosci_PoprawnaIlosc()
        {
            Assert.AreEqual(4, Enums.JednostkiPredkosci.Count);
        }

        [Test]
        public void EnumsTesty_ListaJednostekWydatku_PoprawnaIlosc()
        {
            Assert.AreEqual(5, Enums.JednostkiWydatku.Count);
        }


    }
}

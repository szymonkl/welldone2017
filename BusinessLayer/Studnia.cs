﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using BusinessLayer;
using BusinessLayer.Annotations;
using Stale;
using Wspolne;

namespace Logika
{
    public abstract class Studnia : INotifyPropertyChanged
    {
        private Parametr _wydatek;
        private Parametr _depresjaPozaStudnia;
        private Parametr _promienLejaDepresji;
        private Parametr _promienStudni;
        private Parametr _srednicaStudni;
        private Parametr _wspolczynnikFiltracji;
        private Parametr _miazszoscWarstwy;
        private Parametr _dlugoscCzesciCzynnejFiltra;
        private Parametr _wysokoscH;
        private Enums.SposobyDzialania _sposobDzialania;
        private Parametr _depresjaWStudni;
        private Parametr _poprawkaForhcheimera;
        private Parametr _sprawnosc;
        private Parametr _predkoscDopuszczalna;
        private Parametr _wydatekDopuszczalny;
        private Parametr _depresjaMaksymalnaWStudni;
        private Parametr _depresjaMaksymalnaPozaStudnia;
        private Parametr _promienLejaDepresjiMaksymalny;
        private Parametr _wydatekMaksymalny;

        public string Id { get; }

        public Parametr Wydatek
        {
            get { return _wydatek; }
            set
            {
                if (Equals(value, _wydatek)) return;
                _wydatek = value;
                OnPropertyChanged();
            }
        }

        public Parametr DepresjaPozaStudnia
        {
            get { return _depresjaPozaStudnia; }
            set
            {
                if (Equals(value, _depresjaPozaStudnia)) return;
                _depresjaPozaStudnia = value;
                OnPropertyChanged();
            }
        }

        public Parametr PromienLejaDepresji
        {
            get { return _promienLejaDepresji; }
            set
            {
                if (Equals(value, _promienLejaDepresji)) return;
                _promienLejaDepresji = value;
                OnPropertyChanged();
            }
        }

        public Parametr PromienStudni
        {
            get { return _promienStudni; }
            set
            {
                if (Equals(value, _promienStudni)) return;
                _promienStudni = value;
                OnPropertyChanged();
            }
        }

        public Parametr SrednicaStudni
        {
            get { return _srednicaStudni; }
            set
            {
                if (Equals(value, _srednicaStudni)) return;
                _srednicaStudni = value;
                OnPropertyChanged();
            }
        }

        public Parametr WspolczynnikFiltracji
        {
            get { return _wspolczynnikFiltracji; }
            set
            {
                if (Equals(value, _wspolczynnikFiltracji)) return;
                _wspolczynnikFiltracji = value;
                OnPropertyChanged();
            }
        }

        public Parametr MiazszoscWarstwy
        {
            get { return _miazszoscWarstwy; }
            set
            {
                if (Equals(value, _miazszoscWarstwy)) return;
                _miazszoscWarstwy = value;
                OnPropertyChanged();
            }
        }

        public Parametr DlugoscCzesciCzynnejFiltra
        {
            get { return _dlugoscCzesciCzynnejFiltra; }
            set
            {
                if (Equals(value, _dlugoscCzesciCzynnejFiltra)) return;
                _dlugoscCzesciCzynnejFiltra = value;
                OnPropertyChanged();
            }
        }

        public Parametr WysokoscH
        {
            get { return _wysokoscH; }
            set
            {
                if (Equals(value, _wysokoscH)) return;
                _wysokoscH = value;
                OnPropertyChanged();
            }
        }

        public Enums.SposobyDzialania SposobDzialania
        {
            get { return _sposobDzialania; }
            set
            {
                if (value == _sposobDzialania) return;
                _sposobDzialania = value;
                OnPropertyChanged();
            }
        }

        public Parametr DepresjaWStudni
        {
            get { return _depresjaWStudni; }
            protected set
            {
                if (Equals(value, _depresjaWStudni)) return;
                _depresjaWStudni = value;
                OnPropertyChanged();
            }
        }

        public Parametr PoprawkaForhcheimera
        {
            get { return _poprawkaForhcheimera; }
            protected set
            {
                if (Equals(value, _poprawkaForhcheimera)) return;
                _poprawkaForhcheimera = value;
                OnPropertyChanged();
            }
        }

        public Parametr Sprawnosc
        {
            get { return _sprawnosc; }
            protected set
            {
                if (Equals(value, _sprawnosc)) return;
                _sprawnosc = value;
                OnPropertyChanged();
            }
        }

        public Parametr PredkoscDopuszczalna
        {
            get { return _predkoscDopuszczalna; }
            protected set
            {
                if (Equals(value, _predkoscDopuszczalna)) return;
                _predkoscDopuszczalna = value;
                OnPropertyChanged();
            }
        }

        public Parametr WydatekDopuszczalny
        {
            get { return _wydatekDopuszczalny; }
            protected set
            {
                if (Equals(value, _wydatekDopuszczalny)) return;
                _wydatekDopuszczalny = value;
                OnPropertyChanged();
            }
        }

        public Parametr DepresjaMaksymalnaWStudni
        {
            get { return _depresjaMaksymalnaWStudni; }
            protected set
            {
                if (Equals(value, _depresjaMaksymalnaWStudni)) return;
                _depresjaMaksymalnaWStudni = value;
                OnPropertyChanged();
            }
        }

        public Parametr DepresjaMaksymalnaPozaStudnia
        {
            get { return _depresjaMaksymalnaPozaStudnia; }
            protected set
            {
                if (Equals(value, _depresjaMaksymalnaPozaStudnia)) return;
                _depresjaMaksymalnaPozaStudnia = value;
                OnPropertyChanged();
            }
        }

        public Parametr PromienLejaDepresjiMaksymalny
        {
            get { return _promienLejaDepresjiMaksymalny; }
            protected set
            {
                if (Equals(value, _promienLejaDepresjiMaksymalny)) return;
                _promienLejaDepresjiMaksymalny = value;
                OnPropertyChanged();
            }
        }

        public Parametr WydatekMaksymalny
        {
            get { return _wydatekMaksymalny; }
            protected set
            {
                if (Equals(value, _wydatekMaksymalny)) return;
                _wydatekMaksymalny = value;
                OnPropertyChanged();
            }
        }
       

        protected virtual void InicjalizacjaParametrow()
        {
             var ustawienia = Ustawienia.StworzSingleton();
              ustawienia.Aktualne.PropertyChanged += Ustawienia_PropertyChanged;
              

            Wydatek = new Parametr
            {
                Nazwa = nameof(Wydatek),
                Typ = Enums.TypyParametru.Wydatek,
                Maksimum = ParametryWalidacyjne.Maksymalne[Enums.TypyWalidacyjne.Wydatek],
                Minimum = ParametryWalidacyjne.Minimalne[Enums.TypyWalidacyjne.Wydatek],
                Jednostka = ustawienia.Aktualne.Wydatek
            };
            DepresjaPozaStudnia = new Parametr
            {
                Nazwa = nameof(DepresjaPozaStudnia),
                Typ = Enums.TypyParametru.Dlugosc,
                Maksimum = ParametryWalidacyjne.Maksymalne[Enums.TypyWalidacyjne.Depresja],
                Minimum = ParametryWalidacyjne.Minimalne[Enums.TypyWalidacyjne.Depresja],
                Jednostka = ustawienia.Aktualne.DepresjaPozaStudnia
            };
            PromienLejaDepresji = new Parametr
            {
                Nazwa = nameof(PromienLejaDepresji),
                Typ = Enums.TypyParametru.Dlugosc,
                Maksimum = ParametryWalidacyjne.Maksymalne[Enums.TypyWalidacyjne.PromienLeja],
                Minimum = ParametryWalidacyjne.Minimalne[Enums.TypyWalidacyjne.PromienLeja],
                Jednostka = ustawienia.Aktualne.PromienLejaDepresji
            };
            PromienStudni = new Parametr
            {
                Nazwa = nameof(PromienStudni),
                Typ = Enums.TypyParametru.Dlugosc,
                Maksimum = ParametryWalidacyjne.Maksymalne[Enums.TypyWalidacyjne.PromienStudni],
                Minimum = ParametryWalidacyjne.Minimalne[Enums.TypyWalidacyjne.PromienStudni],
                Jednostka = ustawienia.Aktualne.PromienStudni
            };
            SrednicaStudni = new Parametr
            {
                Nazwa = nameof(SrednicaStudni),
                Typ = Enums.TypyParametru.Dlugosc,
                Maksimum = ParametryWalidacyjne.Maksymalne[Enums.TypyWalidacyjne.PromienStudni],
                Minimum = ParametryWalidacyjne.Minimalne[Enums.TypyWalidacyjne.PromienStudni],
                Jednostka = ustawienia.Aktualne.SrednicaStudni
            };
            WspolczynnikFiltracji = new Parametr
            {
                Nazwa = nameof(WspolczynnikFiltracji),
                Typ = Enums.TypyParametru.Predkosc,
                Maksimum = ParametryWalidacyjne.Maksymalne[Enums.TypyWalidacyjne.WspolczynnikFiltracji],
                Minimum = ParametryWalidacyjne.Minimalne[Enums.TypyWalidacyjne.WspolczynnikFiltracji],
                Jednostka = ustawienia.Aktualne.WspolczynnikFiltracji

            };
            MiazszoscWarstwy = new Parametr
            {
                Nazwa = nameof(MiazszoscWarstwy),
                Typ = Enums.TypyParametru.Dlugosc,
                Maksimum = ParametryWalidacyjne.Maksymalne[Enums.TypyWalidacyjne.Miazszosc],
                Minimum = ParametryWalidacyjne.Minimalne[Enums.TypyWalidacyjne.Miazszosc],
                Jednostka = ustawienia.Aktualne.MiazszoscWarstwy
            };
            DlugoscCzesciCzynnejFiltra = new Parametr
            {
                Nazwa = nameof(DlugoscCzesciCzynnejFiltra),
                Typ = Enums.TypyParametru.Dlugosc,
                Maksimum = ParametryWalidacyjne.Maksymalne[Enums.TypyWalidacyjne.DlugoscFiltra],
                Minimum = ParametryWalidacyjne.Minimalne[Enums.TypyWalidacyjne.DlugoscFiltra],
                Jednostka = ustawienia.Aktualne.DlugoscCzesciCzynnejFiltra,
                PowiazanyParametr = MiazszoscWarstwy,
                WalidacjaInfo = new WalidacjaInfo { JestWiekszeOdPowiazanego = false }
            };
            MiazszoscWarstwy.PowiazanyParametr = DlugoscCzesciCzynnejFiltra;
            MiazszoscWarstwy.WalidacjaInfo = new WalidacjaInfo {JestWiekszeOdPowiazanego = true};
            WysokoscH = new Parametr
            {
                Nazwa = nameof(WysokoscH),
                Typ = Enums.TypyParametru.Dlugosc,
                Maksimum = ParametryWalidacyjne.Maksymalne[Enums.TypyWalidacyjne.Miazszosc],
                Minimum = ParametryWalidacyjne.Minimalne[Enums.TypyWalidacyjne.Miazszosc],
                Jednostka = ustawienia.Aktualne.WysokoscH
            };
            DepresjaWStudni = new Parametr
            {
                Nazwa = nameof(DepresjaWStudni),
                Typ = Enums.TypyParametru.Dlugosc,
                Maksimum = ParametryWalidacyjne.Maksymalne[Enums.TypyWalidacyjne.Depresja],
                Minimum = ParametryWalidacyjne.Minimalne[Enums.TypyWalidacyjne.Depresja],
                Jednostka = ustawienia.Aktualne.DepresjaWStudni
            };
            PoprawkaForhcheimera = new Parametr
            {
                Nazwa = nameof(PoprawkaForhcheimera),
                Typ = Enums.TypyParametru.Brak,
                Maksimum = ParametryWalidacyjne.Maksymalne[Enums.TypyWalidacyjne.PoprawkaForhcheimera],
                Minimum = ParametryWalidacyjne.Minimalne[Enums.TypyWalidacyjne.PoprawkaForhcheimera],
                Jednostka = ustawienia.Aktualne.PoprawkaForhcheimera
            };
            Sprawnosc = new Parametr
            {
                Nazwa = nameof(Sprawnosc),
                Typ = Enums.TypyParametru.Brak,
                Maksimum = ParametryWalidacyjne.Maksymalne[Enums.TypyWalidacyjne.Sprawnosc],
                Minimum = ParametryWalidacyjne.Minimalne[Enums.TypyWalidacyjne.Sprawnosc],
                Jednostka = ustawienia.Aktualne.Sprawnosc
            };
            PredkoscDopuszczalna = new Parametr
            {
                Nazwa = nameof(PredkoscDopuszczalna),
                Typ = Enums.TypyParametru.Predkosc,
                Maksimum = ParametryWalidacyjne.Maksymalne[Enums.TypyWalidacyjne.PredkoscWody],
                Minimum = ParametryWalidacyjne.Minimalne[Enums.TypyWalidacyjne.PredkoscWody],
                Jednostka = ustawienia.Aktualne.PredkoscDopuszczalna
            };
            WydatekDopuszczalny = new Parametr
            {
                Nazwa = nameof(WydatekDopuszczalny),
                Typ = Enums.TypyParametru.Wydatek,
                Maksimum = ParametryWalidacyjne.Maksymalne[Enums.TypyWalidacyjne.Wydatek],
                Minimum = ParametryWalidacyjne.Minimalne[Enums.TypyWalidacyjne.Wydatek],
                Jednostka = ustawienia.Aktualne.WydatekDopuszczalny
            };
            DepresjaMaksymalnaWStudni = new Parametr
            {
                Nazwa = nameof(DepresjaMaksymalnaWStudni),
                Typ = Enums.TypyParametru.Dlugosc,
                Maksimum = ParametryWalidacyjne.Maksymalne[Enums.TypyWalidacyjne.Depresja],
                Minimum = ParametryWalidacyjne.Minimalne[Enums.TypyWalidacyjne.Depresja],
                Jednostka = ustawienia.Aktualne.DepresjaMaksymalnaWStudni
            };
            DepresjaMaksymalnaPozaStudnia = new Parametr
            {
                Nazwa = nameof(DepresjaMaksymalnaPozaStudnia),
                Typ = Enums.TypyParametru.Dlugosc,
                Maksimum = ParametryWalidacyjne.Maksymalne[Enums.TypyWalidacyjne.Depresja],
                Minimum = ParametryWalidacyjne.Minimalne[Enums.TypyWalidacyjne.Depresja],
                Jednostka = ustawienia.Aktualne.DepresjaMaksymalnaPozaStudnia
            };
            PromienLejaDepresjiMaksymalny = new Parametr
            {
                Nazwa = nameof(PromienLejaDepresjiMaksymalny),
                Typ = Enums.TypyParametru.Dlugosc,
                Maksimum = ParametryWalidacyjne.Maksymalne[Enums.TypyWalidacyjne.PromienLeja],
                Minimum = ParametryWalidacyjne.Minimalne[Enums.TypyWalidacyjne.PromienLeja],
                Jednostka = ustawienia.Aktualne.PromienLejaDepresjiMaksymalny
            };
            WydatekMaksymalny = new Parametr
            {
                Nazwa = nameof(WydatekMaksymalny),
                Typ = Enums.TypyParametru.Wydatek,
                Maksimum = ParametryWalidacyjne.Maksymalne[Enums.TypyWalidacyjne.Wydatek],
                Minimum = ParametryWalidacyjne.Minimalne[Enums.TypyWalidacyjne.Wydatek],
                Jednostka = ustawienia.Aktualne.WydatekMaksymalny
            };
            ListaParametrow.ForEach(p => p.PropertyChanged += StudniaPropertyChanged);

        }

        public List<Parametr> ListaParametrow
        {
            get
            {
                var wlasciwosci = GetType().GetProperties().Where(p => p.PropertyType == typeof(Parametr))
                    .Where(p => !p.GetCustomAttributes<IgnorujAttribute>().Any());
                return wlasciwosci.Select(wlasciwosc => wlasciwosc.GetValue(this)).OfType<Parametr>().ToList();
            }
        }

        protected void Ustawienia_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            var zmienionyParamer = ListaParametrow.SingleOrDefault(p => p.Nazwa == e.PropertyName);
            if (zmienionyParamer != null)
            {
                var changedEventArgs = e as UstawieniaChangedEventArgs;
                if (changedEventArgs != null)
                {
                    var nowaJednostka = changedEventArgs.Ustawienie as Jednostka;
                    KonwerterJednostek konwerter = new KonwerterJednostek();
                    konwerter.Konwertuj(zmienionyParamer, nowaJednostka);
                    zmienionyParamer.Jednostka = nowaJednostka;
                   
                }
            }
        }

        protected Studnia()
        {
            Id = Guid.NewGuid().ToString();
        }

        public void StudniaPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged((sender as Parametr)?.Nazwa);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

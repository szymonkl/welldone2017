﻿using System;
using Wspolne;

namespace Logowanie
{
    public class Blad
    {
        public int Id => _id;
        public string IdInstancji { get; set; }
        public DateTime CzasWystapienia { get; }
        public string Wiadomosc { get; set; }
        public string PelnaWiadomosc { get; set; }
        public Enums.TypyBledow Typ { get; set; }
        public Exception Wyjatek { get; set; }

        private int _id;
        private static int _licznik;

        public Blad()
        {
            CzasWystapienia = DateTime.Now;
            _id = _licznik;
            _licznik++;
        }
    }
}

﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using BusinessLayer;
using BusinessLayer.Annotations;
using Logika;
using WellDoneApp.Commands;
using Logowanie;

namespace WellDoneApp.ViewModels
{
    public class StudniaNaporowaViewModel : INotifyPropertyChanged
    {
        private StudniaNaporowa _studnia;

        public StudniaNaporowa Studnia
        {
            get => _studnia;
            set
            {
                if (Equals(value, _studnia)) return;
                _studnia = value;
               OnPropertyChanged();
            }
        }

        public Logger Logger { get; set; }
        public ErrorManager ErrorManager { get; set; }
        public ICommand ObliczeniaCommand { get; private set; }
        public ObservableCollection<Blad> Bledy { get; set; }
        

        public StudniaNaporowaViewModel()
        {
            Studnia = new StudniaNaporowa();
            ObliczeniaCommand = new DoCalculationsCommand(DoCalculations, CanDoCalculations);
            Bledy = new ObservableCollection<Blad>(); 
            Logger = Logger.StworzSingleton();
            Logger.ErrorOccurred += Logger_ErrorOccurred;
            ErrorManager = ErrorManager.StworzSingleton();
            ErrorManager.ErrorOccurred += ErrorManager_ErrorOccurred;
            ErrorManager.ErrorDisappeared += ErrorManager_ErrorDisappeared;
        }

        private void ErrorManager_ErrorOccurred(object sender, ErrorInfoEventArgs e)
        {
            if (Bledy.All(b => b.Wiadomosc != e.BladInfo.Wiadomosc))
            {
                Bledy.Add(e.BladInfo);
            }
        }

        private void ErrorManager_ErrorDisappeared(object sender, ErrorInfoEventArgs e)
        {
           Blad blad = Bledy.FirstOrDefault(b => b.Wiadomosc == e.BladInfo.Wiadomosc);
            if (blad != null)
            {
                Bledy.Remove(blad);
            }
        }

       

        private void Logger_ErrorOccurred(object sender, ErrorInfoEventArgs e)
        {
            if (Bledy.All(b => b.Wiadomosc != e.BladInfo.Wiadomosc))
            {
                Bledy.Add(e.BladInfo);
            }
        }

        private bool CanDoCalculations(object parameter)
        {
            return true;
        }

        private void DoCalculations(object parameter)
        {
            StudniaObliczenia obliczenia = new StudniaNaporowaObliczenia(Studnia);
            obliczenia.Wykonaj();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

﻿using System;

namespace Logowanie
{
    public interface INotifyErrorDisappeared
    {
        event EventHandler<ErrorInfoEventArgs> ErrorDisappeared;
    }
}

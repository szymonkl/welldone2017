﻿using System;

namespace Logowanie
{
    public class ErrorInfoEventArgs : EventArgs
    {
        public Blad BladInfo { get; set; }
    }
}
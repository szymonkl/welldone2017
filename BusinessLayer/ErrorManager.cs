﻿using System;
using Logowanie;

namespace BusinessLayer
{
    public class ErrorManager : INotifyErrorOccurred, INotifyErrorDisappeared
    {
        private static ErrorManager _instancja;
        private static readonly object Blokada = new object();

        private ErrorManager()
        {

        }

        public static ErrorManager StworzSingleton()
        {
            if (_instancja == null)
            {
                lock (Blokada)
                {
                    if (_instancja == null)
                    {
                        _instancja = new ErrorManager();
                    }
                }
            }

            return _instancja;
        }



        public event EventHandler<ErrorInfoEventArgs> ErrorOccurred;
        public event EventHandler<ErrorInfoEventArgs> ErrorDisappeared;

        protected virtual void OnErrorOccurred(ErrorInfoEventArgs e)
        {
            ErrorOccurred?.Invoke(this, e);
        }

        protected virtual void OnErrorDisappeared(ErrorInfoEventArgs e)
        {
            ErrorDisappeared?.Invoke(this, e);
        }

        public void AddError(Blad blad)
        {
            OnErrorOccurred( new ErrorInfoEventArgs { BladInfo = blad});
        }

        public void RemoveError(Blad blad)
        {
            OnErrorDisappeared( new ErrorInfoEventArgs { BladInfo = blad});
        }

    }
}

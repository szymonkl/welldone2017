﻿using Caliburn.Micro;

namespace WellDone2017.ViewModels
{
    internal class Window1ViewModel : PropertyChangedBase
    {
        public Window1ViewModel()
        {
            _name = "Hello World!";
            _dupa = 99;
        }

        private string _name;
        private int _dupa;

        public string Name
        {
            get => _name;
            set
            {
                _name = value;
                NotifyOfPropertyChange(() => Name);
            }
        }


        public int Dupa
        {
            get { return _dupa; }
            set
            {
                _dupa = value;
                NotifyOfPropertyChange(() => Dupa);
            }
        }
    }
}
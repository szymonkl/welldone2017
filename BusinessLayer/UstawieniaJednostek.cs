﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using Logika.Annotations;
using Wspolne;

namespace Logika
{
    public class UstawieniaJednostek : INotifyPropertyChanged
    {
        public Jednostka Wydatek
        {
            get => _wydatek;
            set
            {
                if (Equals(value, _wydatek)) return;
                _wydatek = value;
                OnPropertyChanged();
            }
        }
        public Jednostka DepresjaPozaStudnia
        {
            get => _depresjaPozaStudnia;
            set
            {
                if (Equals(value, _depresjaPozaStudnia)) return;
                _depresjaPozaStudnia = value;
                OnPropertyChanged();
            }
        }
        public Jednostka PromienLejaDepresji
        {
            get => _promienLejaDepresji;
            set
            {
                if (Equals(value, _promienLejaDepresji)) return;
                _promienLejaDepresji = value;
                OnPropertyChanged();
            }
        }
        public Jednostka PromienStudni
        {
            get => _promienStudni;
            set
            {
                if (Equals(value, _promienStudni)) return;
                _promienStudni = value;
                OnPropertyChanged();
            }
        }
        public Jednostka SrednicaStudni
        {
            get => _srednicaStudni;
            set
            {
                if (Equals(value, _srednicaStudni)) return;
                _srednicaStudni = value;
                OnPropertyChanged();
            }
        }
        public Jednostka WspolczynnikFiltracji
        {
            get => _wspolczynnikFiltracji;
            set
            {
                if (Equals(value, _wspolczynnikFiltracji)) return;
                _wspolczynnikFiltracji = value;
                OnPropertyChanged();
            }
        }
        public Jednostka MiazszoscWarstwy
        {
            get => _miazszoscWarstwy;
            set
            {
                if (Equals(value, _miazszoscWarstwy)) return;
                _miazszoscWarstwy = value;
                OnPropertyChanged();
            }
        }
        public Jednostka DlugoscCzesciCzynnejFiltra
        {
            get => _dlugoscCzesciCzynnejFiltra;
            set
            {
                if (Equals(value, _dlugoscCzesciCzynnejFiltra)) return;
                _dlugoscCzesciCzynnejFiltra = value;
                OnPropertyChanged();
            }
        }
        public Jednostka WysokoscH
        {
            get => _wysokoscH;
            set
            {
                if (Equals(value, _wysokoscH)) return;
                _wysokoscH = value;
                OnPropertyChanged();
            }
        }
        public Jednostka DepresjaWStudni
        {
            get => _depresjaWStudni;
            set
            {
                if (Equals(value, _depresjaWStudni)) return;
                _depresjaWStudni = value;
                OnPropertyChanged();
            }
        }
        public Jednostka PoprawkaForhcheimera
        {
            get => _poprawkaForhcheimera;
            set
            {
                if (Equals(value, _poprawkaForhcheimera)) return;
                _poprawkaForhcheimera = value;
                OnPropertyChanged();
            }
        }
        public Jednostka Sprawnosc
        {
            get => _sprawnosc;
            set
            {
                if (Equals(value, _sprawnosc)) return;
                _sprawnosc = value;
                OnPropertyChanged();
            }
        }
        public Jednostka PredkoscDopuszczalna
        {
            get => _predkoscDopuszczalna;
            set
            {
                if (Equals(value, _predkoscDopuszczalna)) return;
                _predkoscDopuszczalna = value;
                OnPropertyChanged();
            }
        }
        public Jednostka WydatekDopuszczalny
        {
            get => _wydatekDopuszczalny;
            set
            {
                if (Equals(value, _wydatekDopuszczalny)) return;
                _wydatekDopuszczalny = value;
                OnPropertyChanged(nameof(WydatekDopuszczalny));
            }
        }
        public Jednostka DepresjaMaksymalnaWStudni
        {
            get => _depresjaMaksymalnaWStudni;
            set
            {
                if (Equals(value, _depresjaMaksymalnaWStudni)) return;
                _depresjaMaksymalnaWStudni = value;
                OnPropertyChanged();
            }
        }
        public Jednostka DepresjaMaksymalnaPozaStudnia
        {
            get => _depresjaMaksymalnaPozaStudnia;
            set
            {
                if (Equals(value, _depresjaMaksymalnaPozaStudnia)) return;
                _depresjaMaksymalnaPozaStudnia = value;
                OnPropertyChanged();
            }
        }
        public Jednostka PromienLejaDepresjiMaksymalny
        {
            get => _promienLejaDepresjiMaksymalny;
            set
            {
                if (Equals(value, _promienLejaDepresjiMaksymalny)) return;
                _promienLejaDepresjiMaksymalny = value;
                OnPropertyChanged();
            }
        }
        public Jednostka WydatekMaksymalny
        {
            get => _wydatekMaksymalny;
            set
            {
                if (Equals(value, _wydatekMaksymalny)) return;
                _wydatekMaksymalny = value;
                OnPropertyChanged();
            }
        }
        public Jednostka WysokoscDeltaH
        {
            get => _wysokoscDeltaH;
            set
            {
                if (Equals(value, _wysokoscDeltaH)) return;
                _wysokoscDeltaH = value;
                OnPropertyChanged();
            }
        }
        public Jednostka WysokoscHMaksymalna
        {
            get => _wysokoscHMaksymalna;
            set
            {
                if (Equals(value, _wysokoscHMaksymalna)) return;
                _wysokoscHMaksymalna = value;
                OnPropertyChanged();
            }
        }
        public Jednostka WysokoscHEksploatacyjna
        {
            get => _wysokoscHEksploatacyjna;
            set
            {
                if (Equals(value, _wysokoscHEksploatacyjna)) return;
                _wysokoscHEksploatacyjna = value;
                OnPropertyChanged();
            }
        }
        public Jednostka PoprawkaForhcheimeraMaksymalna
        {
            get => _poprawkaForhcheimeraMaksymalna;
            set
            {
                if (Equals(value, _poprawkaForhcheimeraMaksymalna)) return;
                _poprawkaForhcheimeraMaksymalna = value;
                OnPropertyChanged();
            }
        }
        public Jednostka WysokoscHsMaksymalna
        {
            get => _wysokoscHsMaksymalna;
            set
            {
                if (Equals(value, _wysokoscHsMaksymalna)) return;
                _wysokoscHsMaksymalna = value;
                OnPropertyChanged();
            }
        }
        public Jednostka WysokoscHsEksploatacyjna
        {
            get => _wysokoscHsEksploatacyjna;
            set
            {
                if (Equals(value, _wysokoscHsEksploatacyjna)) return;
                _wysokoscHsEksploatacyjna = value;
                OnPropertyChanged();
            }
        }
        
        public Dictionary<string, Jednostka> JednostkiParametrow
        {
            get
            {
                var wlasciwosci = GetType().GetProperties().Where(p => p.PropertyType == typeof(Jednostka));
                return wlasciwosci.ToDictionary(p => p.Name, v => v.GetValue(this) as Jednostka);
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
       
        private Jednostka _wydatek = new Jednostka(Enums.Jednostki.Metr3Doba);
        private Jednostka _depresjaPozaStudnia = new Jednostka(Enums.Jednostki.Metr);
        private Jednostka _promienLejaDepresji = new Jednostka(Enums.Jednostki.Metr);
        private Jednostka _promienStudni = new Jednostka(Enums.Jednostki.Cal);
        private Jednostka _srednicaStudni = new Jednostka(Enums.Jednostki.Cal);
        private Jednostka _wspolczynnikFiltracji = new Jednostka(Enums.Jednostki.MetrDoba);
        private Jednostka _miazszoscWarstwy = new Jednostka(Enums.Jednostki.Metr);
        private Jednostka _dlugoscCzesciCzynnejFiltra = new Jednostka(Enums.Jednostki.Metr);
        private Jednostka _wysokoscH = new Jednostka(Enums.Jednostki.Metr);
        private Jednostka _depresjaWStudni = new Jednostka(Enums.Jednostki.Metr);
        private Jednostka _poprawkaForhcheimera = new Jednostka(Enums.Jednostki.Brak);
        private Jednostka _predkoscDopuszczalna = new Jednostka(Enums.Jednostki.MetrDoba);
        private Jednostka _wydatekDopuszczalny = new Jednostka(Enums.Jednostki.Metr3Doba);
        private Jednostka _depresjaMaksymalnaWStudni = new Jednostka(Enums.Jednostki.Metr);
        private Jednostka _depresjaMaksymalnaPozaStudnia = new Jednostka(Enums.Jednostki.Metr);
        private Jednostka _promienLejaDepresjiMaksymalny = new Jednostka(Enums.Jednostki.Metr);
        private Jednostka _wydatekMaksymalny = new Jednostka(Enums.Jednostki.Metr3Doba);
        private Jednostka _wysokoscDeltaH = new Jednostka(Enums.Jednostki.Metr);
        private Jednostka _wysokoscHMaksymalna = new Jednostka(Enums.Jednostki.Metr);
        private Jednostka _wysokoscHEksploatacyjna = new Jednostka(Enums.Jednostki.Metr);
        private Jednostka _poprawkaForhcheimeraMaksymalna = new Jednostka(Enums.Jednostki.Brak);
        private Jednostka _wysokoscHsMaksymalna = new Jednostka(Enums.Jednostki.Metr);
        private Jednostka _wysokoscHsEksploatacyjna = new Jednostka(Enums.Jednostki.Metr);
        private Jednostka _sprawnosc = new Jednostka(Enums.Jednostki.Procent);

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (propertyName != null)
                PropertyChanged?.Invoke(this, new UstawieniaChangedEventArgs(propertyName)
                {
                    Ustawienie = JednostkiParametrow[propertyName]
                });
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Wspolne;

namespace Logika.Testy
{
    [TestFixture]
    public class ObslugaBledowTesty
    {
        private StudniaNaporowa _studniaNaporowa;
        private StudniaSwobodna _studniaSwobodna;

        [OneTimeSetUp]
        public void StworzStudnie()
        {
            _studniaNaporowa = new StudniaNaporowa
            {
                Wydatek = { Wartosc = 1300 },
                SrednicaStudni = { Wartosc = 2200 }, //2200 cale - za dużo
                MiazszoscWarstwy = { Wartosc = 12 },
                DlugoscCzesciCzynnejFiltra = { Wartosc = 11 },
                WysokoscDeltaH = { Wartosc = 1 },
                WspolczynnikFiltracji = { Wartosc = 36 },
                SposobDzialania = Enums.SposobyDzialania.Ciagly
            };

            _studniaSwobodna = new StudniaSwobodna
            {
                Wydatek = { Wartosc = 1000 },
                PromienStudni = { Wartosc = 1000 }, //1000 cali - za dużo
                DlugoscCzesciCzynnejFiltra = { Wartosc = 13 },
                WspolczynnikFiltracji = { Wartosc = 45 },
                WysokoscH = { Wartosc = 22 },
                SposobDzialania = Enums.SposobyDzialania.Awaryjny
            };
        }

        [Test]
        public void StudniaNaporowa_BladObliczen_Przechwycony()
        {
            StudniaNaporowaObliczenia obliczenia = new StudniaNaporowaObliczenia(_studniaNaporowa);
            Assert.DoesNotThrow(() => obliczenia.Wykonaj());
        }

        [Test]
        public void StudniaSwobodna_BladObliczen_Przechwycony()
        {
            StudniaSwobodnaObliczenia obliczenia = new StudniaSwobodnaObliczenia(_studniaSwobodna);
            Assert.DoesNotThrow(() => obliczenia.Wykonaj());
        }




    }
}

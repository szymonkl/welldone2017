﻿using System;

namespace Wspolne
{
    public class TypJednostkiAttribute : Attribute
    {
        public TypJednostkiAttribute(Enums.TypyParametru typ)
        {
            TypParametru = typ;
        }

        public Enums.TypyParametru TypParametru { get; set; }
    }
}

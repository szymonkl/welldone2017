﻿using System;
using System.Windows.Input;

namespace WellDoneApp.Tabs
{
    internal class Tab : ITab
    {
        public Tab()
        {
            CloseCommand =  new ActionCommand(p => CloseRequested?.Invoke(this, EventArgs.Empty));
        }

        public string Name { get; set; }
        public string Title { get; set; }
        public ICommand CloseCommand { get; set; }
        public event EventHandler CloseRequested;
    }
}

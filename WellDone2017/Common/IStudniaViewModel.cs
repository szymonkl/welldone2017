﻿namespace WellDone2017.Common
{
    interface IStudniaViewModel
    {
        string Wydatek { get; set; }
        string WydatekJednostka { get; set; }
        string SrednicaStudni { get; set; }
        string SrednicaStudniJednostka { get; set; }
        string WspolczynnikFiltracji { get; set; }
        string WspolczynnikFiltracjiJednostka { get; set; }
        string MiazszoscWarstwy { get; set; }
        string MiazszoscWarstwyJednostka { get; set; }
        string DlugoscCzesciCzynnejFiltra { get; set; }
        string DlugoscCzesciCzynnejFiltraJednostka { get; set; }
        string WysokoscDeltaH { get; set; }
        string WysokoscDeltaHJednostka { get; set; }

    }
}

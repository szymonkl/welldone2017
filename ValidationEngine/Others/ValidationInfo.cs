﻿using System.Collections.Generic;
using Common;

namespace ValidationEngine.Others
{
    public class ValidationInfo
    {
        public string Name { get; set; }
        public List<string> Message { get; set; } = new List<string>();
        public Enums.TypyWalidacyjne TypWalidacyjny { get; set; }
    }
}
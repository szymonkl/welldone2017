﻿using System.ComponentModel;
using BusinessLayer;
using Stale;
using Wspolne;

namespace Logika
{
    public class StudniaNaporowa : Studnia
    {
        private Parametr _wysokoscDeltaH;

        public Parametr WysokoscDeltaH
        {
            get { return _wysokoscDeltaH; }
            set
            {
                if (Equals(value, _wysokoscDeltaH)) return;
                _wysokoscDeltaH = value;
                OnPropertyChanged();
            }
        }

        public StudniaNaporowa()
        {
            InicjalizacjaParametrow();
        }

        protected sealed override void InicjalizacjaParametrow()
        {
            Ustawienia ustawienia = Ustawienia.StworzSingleton();
            base.InicjalizacjaParametrow();
            WysokoscDeltaH = new Parametr
            {
                Nazwa = nameof(WysokoscDeltaH),
                Typ = Enums.TypyParametru.Dlugosc,
                Maksimum = ParametryWalidacyjne.Maksymalne[Enums.TypyWalidacyjne.Miazszosc],
                Minimum = ParametryWalidacyjne.Minimalne[Enums.TypyWalidacyjne.Miazszosc],
                Jednostka = ustawienia.Aktualne.WysokoscDeltaH
            };
            WysokoscDeltaH.PropertyChanged += StudniaPropertyChanged;

        }

    }

}

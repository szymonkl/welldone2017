﻿using System;
using System.Collections;
using Logika;

namespace Logika
{
    public class DoublePorownywacz : IPorownywacz
    {
        public bool SaRowne(object obiekt1, object obiekt2, object precyzja)
        {
            return Math.Abs((double)obiekt1 - (double)obiekt2) < (double) precyzja;
        }
    }
}

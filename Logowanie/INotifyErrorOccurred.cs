﻿using System;

namespace Logowanie
{
    public interface INotifyErrorOccurred
    {
        event EventHandler<ErrorInfoEventArgs> ErrorOccurred;
    }
}

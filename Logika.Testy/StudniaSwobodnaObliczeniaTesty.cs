﻿using NUnit.Framework;
using Wspolne;

namespace Logika.Testy
{
    [TestFixture]
    public class StudniaSwobodnaObliczeniaTesty
    {
        private StudniaSwobodna _studnia;

        [SetUp]
        public void Start()
        {
            _studnia = new StudniaSwobodna
            {
                Wydatek = { Wartosc = 1000 },
                PromienStudni = { Wartosc = 10}, //10 cali
                DlugoscCzesciCzynnejFiltra = { Wartosc = 13 },
                WspolczynnikFiltracji = { Wartosc = 45 },
                WysokoscH = { Wartosc = 22},
                SposobDzialania = Enums.SposobyDzialania.Awaryjny
            };

            StudniaObliczenia obliczenia = new StudniaSwobodnaObliczenia(_studnia);
            obliczenia.Wykonaj();

        }

        [Test]
        public void StudniaSwobodna_PredkoscDopuszczalna_JestOK()
        {
            Assert.That(_studnia.PredkoscDopuszczalna.Wartosc, Is.EqualTo(131.48).Within(0.01));
        }

        [Test]
        public void StudniaSwobodna_WydatekDopuszczalny_JestOK()
        {
            Assert.That(_studnia.WydatekDopuszczalny.Wartosc, Is.EqualTo(2727.84).Within(0.01));
        }

        [Test]
        public void StudniaSwobodna_PromienMaksymalny_JestOK()
        {
            Assert.That(_studnia.PromienLejaDepresjiMaksymalny.Wartosc, Is.EqualTo(551.35).Within(0.1));
        }

        [Test]
        public void StudniaSwobodna_DepresjaWStudniMaksymalna_JestOK()
        {
            Assert.That(_studnia.DepresjaMaksymalnaWStudni.Wartosc, Is.EqualTo(8.80).Within(0.01));
        }

        [Test]
        public void StudniaSwobodna_DepresjaPozaStudniaMaksymalna_JestOK()
        {
            Assert.That(_studnia.DepresjaMaksymalnaPozaStudnia.Wartosc, Is.EqualTo(8.76).Within(0.01));
        }

        [Test]
        public void StudniaSwobodna_WydatekMaksymalny_JestOK()
        {
            Assert.That(_studnia.WydatekMaksymalny.Wartosc, Is.EqualTo(5681.22).Within(1));
        }

        [Test]
        public void StudniaSwobodna_WysokoscHsMaksymalna_JestOK()
        {
            Assert.That(_studnia.WysokoscHsMaksymalna.Wartosc, Is.EqualTo(13.20).Within(0.01));
        }

        [Test]
        public void StudniaSwobodna_WysokoscHMaksymalna_JestOK()
        {
            Assert.That(_studnia.WysokoscHMaksymalna.Wartosc, Is.EqualTo(13.24).Within(0.01));
        }

        [Test]
        public void StudniaSwobodna_PoprawkaForhcheimeraMaksymalna_JestOK()
        {
            Assert.That(_studnia.PoprawkaForhcheimeraMaksymalna.Wartosc, Is.EqualTo(0.9967).Within(0.01));
        }

        [Test]
        public void StudniaSwobodna_PromienEksploatacyjny_JestOK()
        {
            Assert.That(_studnia.PromienLejaDepresji.Wartosc, Is.EqualTo(55.64).Within(0.1));
        }

        [Test]
        public void StudniaSwobodna_DepresjaWStudniEksploatacyjna_JestOK()
        {
            Assert.That(_studnia.DepresjaWStudni.Wartosc, Is.EqualTo(1.04).Within(0.01));
        }

        [Test]
        public void StudniaSwobodna_DepresjaPozaStudniaEksploatacyjna_JestOK()
        {
            Assert.That(_studnia.DepresjaPozaStudnia.Wartosc, Is.EqualTo(0.88).Within(0.01));
        }

        [Test]
        public void StudniaSwobodna_WydatekEksploatacyjny_JestOK()
        {
            Assert.That(_studnia.Wydatek.Wartosc, Is.EqualTo(1000).Within(1));
        }

        [Test]
        public void StudniaSwobodna_PoprawkaForhcheimera_JestOK()
        {
            Assert.That(_studnia.PoprawkaForhcheimera.Wartosc, Is.EqualTo(0.8515).Within(0.01));
        }

        [Test]
        public void StudniaSwobodna_SprawnoscStudni_JestOK()
        {
            Assert.That(_studnia.Sprawnosc.Wartosc, Is.EqualTo(84.84).Within(1.0));
        }

        [Test]
        public void StudniaSwobodna_WysokoscHsEksploatacyjna_JestOK()
        {
            Assert.That(_studnia.WysokoscHsEksploatacyjna.Wartosc, Is.EqualTo(20.96).Within(0.01));
        }

        [Test]
        public void StudniaSwobodna_WysokoscHEksploatacyjna_JestOK()
        {
            Assert.That(_studnia.WysokoscHEksploatacyjna.Wartosc, Is.EqualTo(21.12).Within(0.01));
        }
    }
}

﻿using System;
using System.Globalization;
using Caliburn.Micro;
using Logika;
using WellDone2017.Common;

namespace WellDone2017.ViewModels
{
    public class StudniaNaporowaWejscieViewModel : PropertyChangedBase, IStudniaViewModel
    {
        private readonly StudniaNaporowa _studnia = new StudniaNaporowa();

        public string Wydatek
        {
            get => _studnia.Wydatek.Wartosc.ToString(CultureInfo.CurrentCulture);
            set
            {
                _studnia.Wydatek.Wartosc = Convert.ToDouble(value);
                NotifyOfPropertyChange(() => Wydatek);
            }
        }

        public string WydatekJednostka
        {
            get => _studnia.Wydatek.Jednostka.Opis;
            set
            {
                _studnia.Wydatek.Jednostka.Opis = value;
                NotifyOfPropertyChange(() => WydatekJednostka);
            }
        }

        public string SrednicaStudni
        {
            get => _studnia.SrednicaStudni.Wartosc.ToString(CultureInfo.CurrentCulture);
            set
            {
                _studnia.SrednicaStudni.Jednostka.Opis = value;
                NotifyOfPropertyChange(() => SrednicaStudni);
            }
        }

        public string SrednicaStudniJednostka
        {
            get => _studnia.SrednicaStudni.Jednostka.Opis;
            set
            {
                _studnia.SrednicaStudni.Jednostka.Opis = value;
                NotifyOfPropertyChange(() => SrednicaStudniJednostka);
            }
        }

        public string WspolczynnikFiltracji
        {
            get => _studnia.WspolczynnikFiltracji.Wartosc.ToString(CultureInfo.CurrentCulture);
            set
            {
                _studnia.WspolczynnikFiltracji.Wartosc = Convert.ToDouble(value);
                NotifyOfPropertyChange(() => WspolczynnikFiltracji);
            }
        }

        public string WspolczynnikFiltracjiJednostka
        {
            get => _studnia.WspolczynnikFiltracji.Jednostka.Opis;
            set
            {
                _studnia.WspolczynnikFiltracji.Jednostka.Opis = value;
                NotifyOfPropertyChange(() => WspolczynnikFiltracjiJednostka);
            }
        }

        public string MiazszoscWarstwy
        {
            get => _studnia.MiazszoscWarstwy.Wartosc.ToString(CultureInfo.CurrentCulture);
            set
            {
                _studnia.MiazszoscWarstwy.Wartosc = Convert.ToDouble(value); 
                NotifyOfPropertyChange(() => MiazszoscWarstwy);
            }
        }

        public string MiazszoscWarstwyJednostka
        {
            get => _studnia.MiazszoscWarstwy.Jednostka.Opis;
            set
            {
                _studnia.MiazszoscWarstwy.Jednostka.Opis = value;
                NotifyOfPropertyChange(() => MiazszoscWarstwyJednostka);
            }
        }

        public string DlugoscCzesciCzynnejFiltra
        {
            get => _studnia.DlugoscCzesciCzynnejFiltra.Wartosc.ToString(CultureInfo.CurrentCulture);
            set
            {
                _studnia.DlugoscCzesciCzynnejFiltra.Wartosc = Convert.ToDouble(value);
                NotifyOfPropertyChange(() => DlugoscCzesciCzynnejFiltra);
            }
        }

        public string DlugoscCzesciCzynnejFiltraJednostka
        {
            get => _studnia.DlugoscCzesciCzynnejFiltra.Jednostka.Opis;
            set
            {
                _studnia.DlugoscCzesciCzynnejFiltra.Jednostka.Opis = value;
                NotifyOfPropertyChange(() => DlugoscCzesciCzynnejFiltraJednostka);
            }
        }

        public string WysokoscDeltaH
        {
            get => _studnia.WysokoscDeltaH.Wartosc.ToString(CultureInfo.CurrentCulture);
            set
            {
                _studnia.WysokoscDeltaH.Wartosc = Convert.ToDouble(value);
                NotifyOfPropertyChange(() => WysokoscDeltaH);
            }
        }

        public string WysokoscDeltaHJednostka
        {
            get => _studnia.WysokoscDeltaH.Jednostka.Opis;
            set
            {
                _studnia.WysokoscDeltaH.Jednostka.Opis = value;
                NotifyOfPropertyChange(() => WysokoscDeltaHJednostka);
            }
        }
    }
}

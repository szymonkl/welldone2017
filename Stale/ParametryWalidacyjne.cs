﻿using System.Collections.Generic;
using Wspolne;

namespace Stale
{
    public static class ParametryWalidacyjne
    {
        public static Dictionary<Enums.TypyWalidacyjne, double> Minimalne { get; set; }
        public static Dictionary<Enums.TypyWalidacyjne, double> Maksymalne { get; set; }
       


        static ParametryWalidacyjne()
        {
            Minimalne = new Dictionary<Enums.TypyWalidacyjne, double>
             {
                 { Enums.TypyWalidacyjne.Wydatek, 0.1 },
                 { Enums.TypyWalidacyjne.Depresja, 0.01 },
                 { Enums.TypyWalidacyjne.DlugoscFiltra, 0.01 },
                 { Enums.TypyWalidacyjne.Miazszosc, 0.01 },
                 { Enums.TypyWalidacyjne.PoprawkaForhcheimera, 0.001 },
                 { Enums.TypyWalidacyjne.PredkoscWody, 0.01 },
                 { Enums.TypyWalidacyjne.PromienLeja, 1 },
                 { Enums.TypyWalidacyjne.PromienStudni, 1 },
                 { Enums.TypyWalidacyjne.Sprawnosc, 0.001 },
                 { Enums.TypyWalidacyjne.WspolczynnikFiltracji, 0.0000000000001 },
             };

            Maksymalne = new Dictionary<Enums.TypyWalidacyjne, double>
             {
                 { Enums.TypyWalidacyjne.Wydatek, 100000 },
                 { Enums.TypyWalidacyjne.Depresja, 100 },
                 { Enums.TypyWalidacyjne.DlugoscFiltra, 1000 },
                 { Enums.TypyWalidacyjne.Miazszosc, 1000 },
                 { Enums.TypyWalidacyjne.PoprawkaForhcheimera, 1 },
                 { Enums.TypyWalidacyjne.PredkoscWody, 10000 },
                 { Enums.TypyWalidacyjne.PromienLeja, 20000 },
                 { Enums.TypyWalidacyjne.PromienStudni, 100 },
                 { Enums.TypyWalidacyjne.Sprawnosc, 1 },
                 { Enums.TypyWalidacyjne.WspolczynnikFiltracji, 1000 },
             };
        }
    }
}

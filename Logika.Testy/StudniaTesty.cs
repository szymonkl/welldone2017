﻿using NUnit.Framework;

namespace Logika.Testy
{
    [TestFixture]
    public class StudniaTesty
    {
        [Test]
        public void StudniaNaporowa_ListaParametrow_PoprawnaIlosc()
        {
            Studnia studnia = new StudniaNaporowa();
            Assert.AreEqual(19, studnia.ListaParametrow.Count);
        }
        [Test]
        public void StudniaNaporowa_ListaParametrow_PoprawnyTyp()
        {
            Studnia studnia = new StudniaNaporowa();
            Assert.True(studnia.ListaParametrow.TrueForAll(p => p.GetType() == typeof(Parametr)));
        }
        [Test]
        public void StudniaSwobodna_ListaParametrow_PoprawnaIlosc()
        {
            Studnia studnia = new StudniaSwobodna();
            Assert.AreEqual(23, studnia.ListaParametrow.Count);
        }
        [Test]
        public void StudniaSwobodna_ListaParametrow_PoprawnyTyp()
        {
            Studnia studnia = new StudniaSwobodna();
            Assert.True(studnia.ListaParametrow.TrueForAll(p => p.GetType() == typeof(Parametr)));
        }
    }
}

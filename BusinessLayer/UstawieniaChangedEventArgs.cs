﻿using System.ComponentModel;

namespace Logika
{
    public class UstawieniaChangedEventArgs : PropertyChangedEventArgs
    {
        public object Ustawienie { get; set; }
        public UstawieniaChangedEventArgs(string propertyName) : base(propertyName)
        {
            
        }
    }
}

﻿using Common;
using ValidationEngine.Others;

namespace ValidationEngine.ValidationRules
{
    public class DoubleGreaterThanMinimumValidationRule : IValidationRule
    {
        public bool IsValid(object target, ValidationInfo info)
        {
            var value = (double) target;
            var isValid =  value > ValidationParameters.MinValues[info.TypWalidacyjny];
            if (!isValid)
            {
                info.Message.Add(info.Name + " jest za mała!");
            }
            return isValid;
        }
    }
}

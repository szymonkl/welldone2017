﻿namespace Stale
{
    public static class StaleProgramu
    {
        public static double DokladnoscObliczen = 0.001;
        public static double WspolczynnikDepresjiMaksymalnejStudniSwobodnej = 0.4;
        public static double WspolczynnikWysokosciMaksymalnejStudniNaporowej = 0.6;

        public const double DokladnoscObliczenDefault = 0.001;
        public const double WspolczynnikDepresjiMaksymalnejStudniSwobodnejDefault = 0.4;
        public const double WspolczynnikWysokosciMaksymalnejStudniNaporowejDefault = 0.6;
    }
}

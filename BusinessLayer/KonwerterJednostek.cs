﻿namespace Logika
{
    public class KonwerterJednostek
    {
        public void Konwertuj(Parametr parametr, Jednostka jednostka)
        {
            double stalaPoczatkowa = Stale.StaleJednostek.Wszystkie[parametr.Jednostka.Nazwa];
            double stalaKoncowa = Stale.StaleJednostek.Wszystkie[jednostka.Nazwa];
            double wspolczynnik = stalaPoczatkowa / stalaKoncowa;
            parametr.Minimum = parametr.Minimum * wspolczynnik;
            parametr.Maksimum = parametr.Maksimum * wspolczynnik;
            parametr.Wartosc = parametr.Wartosc * wspolczynnik;
            
            parametr.Jednostka = jednostka;
        }
    }
}

﻿using Logika;
using NUnit.Framework;
using Wspolne;

namespace Logika.Testy
{
    [TestFixture]
    public class KonwerterJednostekTesty
    {
        private readonly KonwerterJednostek _konwerter = new KonwerterJednostek();

        [TestCase(Enums.Jednostki.Centymetr, Enums.Jednostki.Metr, 2, 0.02)]
        [TestCase(Enums.Jednostki.Metr, Enums.Jednostki.Centymetr, 2.31, 231)]
        [TestCase(Enums.Jednostki.Metr, Enums.Jednostki.Decymetr, 22.5, 225)]
        [TestCase(Enums.Jednostki.Decymetr, Enums.Jednostki.Metr, 1, 0.1)]
        [TestCase(Enums.Jednostki.Cal, Enums.Jednostki.Centymetr, 1, 2.54)]
        [TestCase(Enums.Jednostki.Decymetr, Enums.Jednostki.Cal, 11, 43.31)]
        public void KonwerterJednostek_JednostkiDlugosci_PrawidloweWartosci(Enums.Jednostki jednostkaPoczatkowa, Enums.Jednostki jednostkaKoncowa, double wartosc, double wynik)
        {
            var parametr = new Parametr {Jednostka = new Jednostka(jednostkaPoczatkowa), Wartosc = wartosc};
            _konwerter.Konwertuj(parametr, new Jednostka(jednostkaKoncowa));
            Assert.That(parametr.Wartosc, Is.EqualTo(wynik).Within(0.01));
            Assert.AreEqual(parametr.Jednostka.Nazwa, jednostkaKoncowa);

        }

        [TestCase(Enums.Jednostki.Sekunda, Enums.Jednostki.Godzina, 1, 1.0/3600)]
        [TestCase(Enums.Jednostki.Godzina, Enums.Jednostki.Sekunda, 2.15, 7740)]
        [TestCase(Enums.Jednostki.Sekunda, Enums.Jednostki.Minuta, 10, 10.0/60)]
        [TestCase(Enums.Jednostki.Minuta, Enums.Jednostki.Sekunda, 100, 6000)]
        [TestCase(Enums.Jednostki.Sekunda, Enums.Jednostki.Sekunda, 123, 123)]
        [TestCase(Enums.Jednostki.Godzina, Enums.Jednostki.Doba, 11, 11.0/24)]
        public void KonwerterJednostek_JednostkiCzasu_PrawidloweWartosci(Enums.Jednostki jednostkaPoczatkowa, Enums.Jednostki jednostkaKoncowa, double wartosc, double wynik)
        {
            var parametr = new Parametr { Jednostka = new Jednostka(jednostkaPoczatkowa), Wartosc = wartosc };
            _konwerter.Konwertuj(parametr, new Jednostka(jednostkaKoncowa));
            Assert.That(parametr.Wartosc, Is.EqualTo(wynik).Within(0.01));
            Assert.AreEqual(parametr.Jednostka.Nazwa, jednostkaKoncowa);

        }

        [TestCase(Enums.Jednostki.MetrSekunda, Enums.Jednostki.MetrDoba, 5, 432000)]
        [TestCase(Enums.Jednostki.MetrDoba, Enums.Jednostki.MetrSekunda, 133000, 1.5394)]
        [TestCase(Enums.Jednostki.MetrGodzina, Enums.Jednostki.MetrSekunda, 1000, 0.2778)]
        [TestCase(Enums.Jednostki.MetrSekunda, Enums.Jednostki.MetrGodzina, 0.1, 360)]
        [TestCase(Enums.Jednostki.CalSekunda, Enums.Jednostki.MetrSekunda, 10, 0.254)]
        [TestCase(Enums.Jednostki.MetrSekunda, Enums.Jednostki.CalSekunda, 2.5, 98.43)]
        public void KonwerterJednostek_JednostkiPredkosci_PrawidloweWartosci(Enums.Jednostki jednostkaPoczatkowa, Enums.Jednostki jednostkaKoncowa, double wartosc, double wynik)
        {
            var parametr = new Parametr { Jednostka = new Jednostka(jednostkaPoczatkowa), Wartosc = wartosc };
            _konwerter.Konwertuj(parametr, new Jednostka(jednostkaKoncowa));
            Assert.That(parametr.Wartosc, Is.EqualTo(wynik).Within(0.01));
            Assert.AreEqual(parametr.Jednostka.Nazwa, jednostkaKoncowa);

        }

        [TestCase(Enums.Jednostki.Metr3Doba, Enums.Jednostki.Metr3Godzina, 120, 5)]
        [TestCase(Enums.Jednostki.Metr3Godzina, Enums.Jednostki.Metr3Doba, 4.5, 108)]
        [TestCase(Enums.Jednostki.GalonDoba, Enums.Jednostki.Metr3Doba, 24, 0.1091)]
        [TestCase(Enums.Jednostki.Metr3Doba, Enums.Jednostki.GalonDoba, 0.5, 109.9846)]
        [TestCase(Enums.Jednostki.GalonDoba, Enums.Jednostki.Metr3Godzina, 10000, 1.8942)]
        [TestCase(Enums.Jednostki.Metr3Godzina, Enums.Jednostki.GalonDoba, 0.01, 52.7928)]
        [TestCase(Enums.Jednostki.LitrSekunda, Enums.Jednostki.Metr3Doba, 1, 86.4)]
        [TestCase(Enums.Jednostki.LitrSekunda, Enums.Jednostki.GalonDoba, 0.009, 171.04608)]
        public void KonwerterJednostek_JednostkiWydatku_PrawidloweWartosci(Enums.Jednostki jednostkaPoczatkowa, Enums.Jednostki jednostkaKoncowa, double wartosc, double wynik)
        {
            var parametr = new Parametr { Jednostka = new Jednostka(jednostkaPoczatkowa), Wartosc = wartosc };
            _konwerter.Konwertuj(parametr, new Jednostka(jednostkaKoncowa));
            Assert.That(parametr.Wartosc, Is.EqualTo(wynik).Within(0.01));
            Assert.AreEqual(parametr.Jednostka.Nazwa, jednostkaKoncowa);

        }

    }
}

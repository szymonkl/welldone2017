﻿using BusinessLayer;
using Stale;
using Wspolne;

namespace Logika
{
    public class StudniaSwobodna : Studnia
    {
        public Parametr WysokoscHMaksymalna { get; set; }
        public Parametr WysokoscHEksploatacyjna { get; set; }
        public Parametr PoprawkaForhcheimeraMaksymalna { get; set; }
        public Parametr WysokoscHsMaksymalna { get; set; }
        public Parametr WysokoscHsEksploatacyjna { get; set; }

        public StudniaSwobodna()
        {
            InicjalizacjaParametrow();
        }

        protected sealed override void InicjalizacjaParametrow()
        {
            Ustawienia ustawienia = Ustawienia.StworzSingleton();
            base.InicjalizacjaParametrow();
            
            WysokoscHMaksymalna = new Parametr
            {
                Nazwa = nameof(WysokoscHMaksymalna),
                Typ = Enums.TypyParametru.Dlugosc,
                Maksimum = ParametryWalidacyjne.Maksymalne[Enums.TypyWalidacyjne.Miazszosc],
                Minimum = ParametryWalidacyjne.Minimalne[Enums.TypyWalidacyjne.Miazszosc],
                Jednostka = ustawienia.Aktualne.WysokoscHMaksymalna
            };
            WysokoscHEksploatacyjna = new Parametr
            {
                Nazwa = nameof(WysokoscHEksploatacyjna),
                Typ = Enums.TypyParametru.Dlugosc,
                Maksimum = ParametryWalidacyjne.Maksymalne[Enums.TypyWalidacyjne.Miazszosc],
                Minimum = ParametryWalidacyjne.Minimalne[Enums.TypyWalidacyjne.Miazszosc],
                Jednostka = ustawienia.Aktualne.WysokoscHEksploatacyjna
            };
            PoprawkaForhcheimeraMaksymalna = new Parametr
            {
                Nazwa = nameof(PoprawkaForhcheimeraMaksymalna),
                Typ = Enums.TypyParametru.Brak,
                Maksimum = ParametryWalidacyjne.Maksymalne[Enums.TypyWalidacyjne.PoprawkaForhcheimera],
                Minimum = ParametryWalidacyjne.Minimalne[Enums.TypyWalidacyjne.PoprawkaForhcheimera],
                Jednostka = ustawienia.Aktualne.PoprawkaForhcheimeraMaksymalna
            };
            WysokoscHsMaksymalna = new Parametr
            {
                Nazwa = nameof(WysokoscHsMaksymalna),
                Typ = Enums.TypyParametru.Dlugosc,
                Maksimum = ParametryWalidacyjne.Maksymalne[Enums.TypyWalidacyjne.Miazszosc],
                Minimum = ParametryWalidacyjne.Minimalne[Enums.TypyWalidacyjne.Miazszosc],
                Jednostka = ustawienia.Aktualne.WysokoscHsMaksymalna
            };
            WysokoscHsEksploatacyjna = new Parametr
            {
                Nazwa = nameof(WysokoscHsEksploatacyjna),
                Typ = Enums.TypyParametru.Dlugosc,
                Maksimum = ParametryWalidacyjne.Maksymalne[Enums.TypyWalidacyjne.Miazszosc],
                Minimum = ParametryWalidacyjne.Minimalne[Enums.TypyWalidacyjne.Miazszosc],
                Jednostka = ustawienia.Aktualne.WysokoscHsEksploatacyjna
            };
        }
    }
}

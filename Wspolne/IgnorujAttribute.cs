﻿using System;

namespace Wspolne
{
    [AttributeUsage(AttributeTargets.Property)]
    public class IgnorujAttribute : Attribute
    {
    }
}

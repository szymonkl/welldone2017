﻿using System.Globalization;
using System.Windows.Controls;
using BusinessLayer;
using Logika;
using Logowanie;

namespace WellDoneApp.Validation
{
    public class DoubleValidationRule : ValidationRule
    {
        public string WiadomoscBledu { get; set; }
       
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            ValidationResult validationResult;

            Parametr parametr = value as Parametr;

            if (parametr != null)
            {
                ErrorManager errorManager = ErrorManager.StworzSingleton();
                if (parametr.Wartosc < parametr.Minimum)
                {
                    WiadomoscBledu = parametr.NazwaWyswietlana  + Slownik.Logika.ZbytMaly;
                    validationResult = new ValidationResult(false, WiadomoscBledu);
                   
                    errorManager.AddError( new Blad { Wiadomosc = WiadomoscBledu});

                    

                }
                else if (parametr.Wartosc > parametr.Maksimum)
                {
                    WiadomoscBledu = parametr.NazwaWyswietlana + Slownik.Logika.ZbytDuzy;
                    validationResult = new ValidationResult(false, WiadomoscBledu);

                    errorManager.AddError(new Blad { Wiadomosc = WiadomoscBledu });
                }
                else
                {
                    validationResult = ValidationResult.ValidResult;

                    errorManager.RemoveError(new Blad { Wiadomosc = WiadomoscBledu });
                } 
            }
            else
            {
                validationResult = new ValidationResult(true, null);
            }
            
            return validationResult;
        }
    }
}

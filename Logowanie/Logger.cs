﻿using System;
using System.IO;
using System.Text;

namespace Logowanie
{
    public class Logger : INotifyErrorOccurred
    {
        public static string SciezkaPlikuLogow => Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
                                     @"\WellDone2017";

        public static string NazwaPlikuLogow => "Logs.txt";

        private static Logger _instancja;
        private static readonly object Blokada = new object();

        private Logger()
        {
            
        }

        public static Logger StworzSingleton()
        {
            if (_instancja == null)
            {
                lock (Blokada)
                {
                    if (_instancja == null)
                    {
                        _instancja = new Logger();
                    }
                }
            }

            return _instancja;
        }

        public void Log(Blad blad)
        {
            StworzWiadomoscBledu(blad);
            ProbujZapisacLog(blad.PelnaWiadomosc);
            ErrorOccurred?.Invoke(this, new ErrorInfoEventArgs { BladInfo = blad });
        }

        private static void StworzWiadomoscBledu(Blad blad)
        {
            var stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("---------------------------------------------------------------");
            stringBuilder.AppendLine("Id błędu: " + blad.Id);
            stringBuilder.AppendLine("Instancja: " + blad.IdInstancji);
            stringBuilder.Append(blad.CzasWystapienia.ToShortDateString());
            stringBuilder.Append(" ");
            stringBuilder.AppendLine(blad.CzasWystapienia.ToLongTimeString());
            stringBuilder.AppendLine(blad.Typ.ToString());
            stringBuilder.AppendLine(blad.Wiadomosc);

            if (blad.Wyjatek != null)
            {
                stringBuilder.AppendLine(blad.Wyjatek.Message);
                stringBuilder.AppendLine(blad.Wyjatek.Source);
                stringBuilder.AppendLine(blad.Wyjatek.StackTrace);
            }
            
            stringBuilder.AppendLine("---------------------------------------------------------------");
            blad.PelnaWiadomosc = stringBuilder.ToString();
        }

        private static void ProbujZapisacLog(string wiadomosc)
        {
            try
            {
                ZapiszLog(wiadomosc);
            }
            catch
            {
                if (!Directory.Exists(SciezkaPlikuLogow))
                {
                    Directory.CreateDirectory(SciezkaPlikuLogow);
                    if (!File.Exists(SciezkaPlikuLogow + @"\"+ NazwaPlikuLogow))
                    {
                        File.Create(SciezkaPlikuLogow + @"\" + NazwaPlikuLogow).Dispose();
                        ZapiszLog(wiadomosc);
                    }
                }
            }
        }

        private static void ZapiszLog(string wiadomosc)
        {
            using (var writer = new StreamWriter(new FileStream(SciezkaPlikuLogow + @"\" + NazwaPlikuLogow, FileMode.Append,
                FileAccess.Write, FileShare.ReadWrite)))
            {
                writer.WriteLine(wiadomosc);
            }
        }

        public event EventHandler<ErrorInfoEventArgs> ErrorOccurred;

        protected virtual void OnErrorOccurred(ErrorInfoEventArgs e)
        {
            ErrorOccurred?.Invoke(this, e);
        }
    }
}


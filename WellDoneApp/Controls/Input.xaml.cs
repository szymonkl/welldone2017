﻿using System.Windows;
using System.Windows.Controls;
using Logika;

namespace WellDone2017.Controls
{
    /// <summary>
    /// Interaction logic for Input.xaml
    /// </summary>
    public partial class Input : UserControl
    {
        public Input()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty ParametrProperty =
            DependencyProperty.Register("Parametr", typeof(Parametr),
                typeof(Input), new FrameworkPropertyMetadata(null));

        public Parametr Parametr
        {
            get { return GetValue(ParametrProperty) as Parametr; }
            set { SetValue(ParametrProperty, value); }
        }
    }
}

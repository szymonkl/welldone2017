﻿using System.Windows;
using System.Windows.Controls;

namespace WellDone2017.Controls
{
    /// <summary>
    /// Interaction logic for Input.xaml
    /// </summary>
    public partial class Input : UserControl
    {
        public Input()
        {
            InitializeComponent();
        }
        public static readonly DependencyProperty ControlNameProperty =
            DependencyProperty.Register("ControlName", typeof(string),
                typeof(Input), new FrameworkPropertyMetadata(null));

        public string ControlName
        {
            get { return GetValue(ControlNameProperty) as string; }
            set { SetValue(ControlNameProperty, value); }
        }
        public static readonly DependencyProperty ControlValueProperty =
            DependencyProperty.Register("ControlValue", typeof(string),
                typeof(Input), new FrameworkPropertyMetadata(null));

        public string ControlValue
        {
            get { return GetValue(ControlValueProperty) as string; }
            set { SetValue(ControlValueProperty, value); }
        }

        public static readonly DependencyProperty ControlUnitProperty =
            DependencyProperty.Register("ControlUnit", typeof(string),
                typeof(Input), new FrameworkPropertyMetadata(null));

        public string ControlUnit
        {
            get { return GetValue(ControlUnitProperty) as string; }
            set { SetValue(ControlUnitProperty, value); }
        }
    }
}

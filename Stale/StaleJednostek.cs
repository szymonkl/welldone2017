﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wspolne;

namespace Stale
{
    public class StaleJednostek
    {
        public static Dictionary<Enums.Jednostki, double> JednostkiDlugosci { get; set; }
        public static Dictionary<Enums.Jednostki, double> JednostkiCzasu { get; set; }
        public static Dictionary<Enums.Jednostki, double> JednostkiPredkosci { get; set; }
        public static Dictionary<Enums.Jednostki, double> JednostkiWydatku { get; set; }
        public static Dictionary<Enums.Jednostki, double> Inne { get; set; }

        public static Dictionary<Enums.Jednostki, double> Wszystkie { get; set; }

        static StaleJednostek()
        {
            JednostkiDlugosci = new Dictionary<Enums.Jednostki, double>
            {
                {Enums.Jednostki.Centymetr, 0.01},
                {Enums.Jednostki.Cal, 0.0254},
                {Enums.Jednostki.Decymetr, 0.1},
                {Enums.Jednostki.Metr, 1},
                {Enums.Jednostki.Kilometr, 1000}
            };
            JednostkiCzasu = new Dictionary<Enums.Jednostki, double>()
            {
                {Enums.Jednostki.Sekunda, 1},
                {Enums.Jednostki.Minuta, 60},
                {Enums.Jednostki.Godzina, 3600},
                {Enums.Jednostki.Doba, 86400}
            };
            JednostkiPredkosci = new Dictionary<Enums.Jednostki, double>()
            {
                {Enums.Jednostki.MetrSekunda, 1},
                {Enums.Jednostki.MetrGodzina, 1.0/3600},
                {Enums.Jednostki.MetrDoba, 1.0/86400},
                {Enums.Jednostki.CalSekunda, 0.0254}

                
            };
            JednostkiWydatku = new Dictionary<Enums.Jednostki, double>()
            {
                {Enums.Jednostki.Metr3Doba, 1.0/86400},
                {Enums.Jednostki.Metr3Godzina, 1.0/3600},
                {Enums.Jednostki.LitrSekunda, 0.001},
                {Enums.Jednostki.GalonDoba, 1.0/86400*0.004546},
                {Enums.Jednostki.GalonGodzina, 1.0/86400*0.004546}
            };
            Inne = new Dictionary<Enums.Jednostki, double>()
            {
                {Enums.Jednostki.Brak, 1},
                {Enums.Jednostki.Procent, 1 }

            };

            Wszystkie = JednostkiDlugosci.Concat(JednostkiCzasu).Concat(JednostkiPredkosci).Concat(JednostkiWydatku).Concat(Inne).ToDictionary(s => s.Key, s => s.Value);
        }

        
    }
}

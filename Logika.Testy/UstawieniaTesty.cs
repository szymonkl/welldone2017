﻿using BusinessLayer;
using NUnit.Framework;
using Wspolne;

namespace Logika.Testy
{
    [TestFixture]
    public class UstawieniaTesty
    {
        private Studnia _studnia;
        private Ustawienia _ustawienia;
        [SetUp]
        public void UstawieniaJednostek_WczytajUstawienia()
        {
            _ustawienia = Ustawienia.StworzSingleton();
        }
        [Test]
        public void UstawieniaJednostek_ListaUstawien_IloscElementow()
        {
            Assert.AreEqual(24, _ustawienia.Aktualne.JednostkiParametrow.Count);
        }

        [Test]
        public void UstawieniaJednostek_ListaUstawien_ZawieraUstawienieJednostkiWydatku()
        {
            Assert.True(_ustawienia.Aktualne.JednostkiParametrow.ContainsKey("Wydatek"));
        }

        [Test]
        public void UstawieniaJednostek_ZmianaUstawienWydatku_ZmianyZachodza()
        {
            _studnia = new StudniaNaporowa();
            _ustawienia.Aktualne.Wydatek = new Jednostka(Enums.Jednostki.Metr3Godzina);
            Assert.AreEqual(Enums.Jednostki.Metr3Godzina, ((StudniaNaporowa)_studnia).Wydatek.Jednostka.Nazwa);
        }

        [Test]
        public void UstawieniaJednostek_ZmianaUstawienWysokosciDeltaH_ZmianyZachodza()
        {
             _studnia = new StudniaNaporowa();
            _ustawienia.Aktualne.WysokoscDeltaH = new Jednostka(Enums.Jednostki.Centymetr);
            Assert.AreEqual(Enums.Jednostki.Centymetr, ((StudniaNaporowa) _studnia).WysokoscDeltaH.Jednostka.Nazwa);
        }

        [Test]
        public void UstawieniaJednostek_ZmianaUstawienWysokosciHsEksploatacyjnej_ZmianyZachodza()
        {
            _studnia = new StudniaSwobodna();
            _ustawienia.Aktualne.WysokoscHsEksploatacyjna = new Jednostka(Enums.Jednostki.Cal);
            Assert.AreEqual(Enums.Jednostki.Cal, ((StudniaSwobodna) _studnia).WysokoscHsEksploatacyjna.Jednostka.Nazwa);
        }

        [Test]
        public void UstawieniaJednostek_ZmianaUstawieniaJednostkiWydatku_WartoscPrzekonwertowana()
        {
            _studnia = new StudniaNaporowa {Wydatek = {Wartosc = 24}};
            _ustawienia.Aktualne.Wydatek = new Jednostka(Enums.Jednostki.Metr3Godzina);

            Assert.AreEqual(Enums.Jednostki.Metr3Godzina, _studnia.Wydatek.Jednostka.Nazwa);
            Assert.AreEqual(1, _studnia.Wydatek.Wartosc);
        }

        [Test]
        public void UstawieniaJednostek_ZmianaUstawieniaJednostkiWysokosciDeltaH_WartoscPrzekonwertowana()
        {
            _studnia = new StudniaNaporowa { WysokoscDeltaH = { Wartosc = 10 } };
            _ustawienia.Aktualne.WysokoscDeltaH = new Jednostka(Enums.Jednostki.Centymetr);

            Assert.AreEqual(Enums.Jednostki.Centymetr, ((StudniaNaporowa) _studnia).WysokoscDeltaH.Jednostka.Nazwa);
            Assert.AreEqual(1000, ((StudniaNaporowa) _studnia).WysokoscDeltaH.Wartosc);
        }

        [Test]
        public void UstawieniaJednostek_ZmianaUstawieniaJednostkiWysokosciHMaksymalnej_WartoscPrzekonwertowana()
        {
            _studnia = new StudniaSwobodna { WysokoscHMaksymalna = { Wartosc = 10 } };
            _ustawienia.Aktualne.WysokoscHMaksymalna = new Jednostka(Enums.Jednostki.Centymetr);

            Assert.AreEqual(Enums.Jednostki.Centymetr, ((StudniaSwobodna)_studnia).WysokoscHMaksymalna.Jednostka.Nazwa);
            Assert.AreEqual(1000, ((StudniaSwobodna)_studnia).WysokoscHMaksymalna.Wartosc);
        }

        [Test]
        public void UstawieniaJednostek_UstawieniaDocelowe_WlasciweJednostki()
        {
            Assert.AreEqual(Enums.Jednostki.Metr, _ustawienia.Docelowe.SrednicaStudni.Nazwa);
            Assert.AreEqual(Enums.Jednostki.Metr, _ustawienia.Docelowe.PromienStudni.Nazwa);
            
        }
    }
}

﻿using System;
using System.IO;
using NUnit.Framework;
using static Wspolne.Enums;

namespace Logowanie.Testy
{
    [TestFixture]
    public class Logger
    {
        private Blad _blad;
        private Logowanie.Logger _logger;
        [OneTimeSetUp]
        public void Logger_RzucWyjatek()
        {
            _logger = Logowanie.Logger.StworzSingleton();
            try
            {
                throw new NullReferenceException();
            }
            catch (Exception e)
            {
                _blad = new Blad();
                _blad.Typ = TypyBledow.BladKrytyczny;
                _blad.Wiadomosc = "Test message";
                _blad.Wyjatek = e;
                _logger.Log(_blad);
            }
        }
        
        [Test]
        public void Logger_Wiadomosc_NieJestPusta()
        {
            Assert.True(!string.IsNullOrWhiteSpace(_blad.Wiadomosc)); 
        }

        [Test]
        public void Logger_Data_JestPoprawna()
        {
            Assert.True(_blad.CzasWystapienia.ToShortDateString() == DateTime.Now.ToShortDateString());
        }

        [Test]
        public void Logger_PelnaWiadomosc_NieJestPusta()
        {
            Assert.True(!string.IsNullOrWhiteSpace(_blad.PelnaWiadomosc));
        }

        [Test]
        public void Logger_PlikLogow_Istnieje()
        {
            Assert.True(File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
                                    @"\WellDone2017\Logs.txt"));
        }

    }
}

﻿using System;
using Logika.Wyjatki;
using Logowanie;
using Wspolne;

namespace Logika
{
    public class StudniaSwobodnaObliczenia : StudniaObliczenia
    {
        private double _hmaxTemp;
        private double _bmaxTemp;
        private double _depresjaTemp;
        private double _promienTemp;


        public StudniaSwobodnaObliczenia(Studnia studnia)
            : base(studnia as StudniaSwobodna)
        { }


        protected override void ObliczParametryMaksymalne()
        {
            Oblicz_hmax_bmax();
            ObliczDepresjeMaksymalnaPozaStudnia();
            ObliczMaksymalnyPromienLejaDepresji();
            ObliczWydatekMaksymalny();
        }

        private void Oblicz_hmax_bmax()
        {
            try
            {
                double starabMax;
                _bmaxTemp = WylosujParametrPoczatkowy(Enums.TypyWalidacyjne.PoprawkaForhcheimera);
                IPorownywacz porownywacz = new DoublePorownywacz();
                ObliczDepresjeMaksymalnaWStudni();
                ObliczWysokoscHsMaksymalna();

                do
                {
                    Oblicz_hmax_temp();
                    if (JestPozaZakresem(_hmaxTemp))
                    {
                        throw new WyjatekObliczen(Slownik.Logika.NieprawidlowaWysokoschMaksymalna);
                    }
                    starabMax = _bmaxTemp;
                    Oblicz_bmax_temp();
                    if (double.IsNaN(_bmaxTemp) || double.IsInfinity(_bmaxTemp))
                    {
                        throw new WyjatekObliczen(Slownik.Logika.NieprawidlowaPoprawkaForhcheimeraMaksymalna);
                    }

                } while (!porownywacz.SaRowne(starabMax, _bmaxTemp, Stale.StaleProgramu.DokladnoscObliczen));

                Oblicz_hmax_temp();

                ((StudniaSwobodna)Studnia).WysokoscHMaksymalna.Wartosc = _hmaxTemp;
                ((StudniaSwobodna)Studnia).PoprawkaForhcheimeraMaksymalna.Wartosc = _bmaxTemp;
            }
            catch (Exception e)
            {
                Logger.Log(new Blad() {IdInstancji = Studnia.Id, Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladObliczen + Slownik.Logika.PoprawkaForhcheimeraMaksymalna });
            }
        }

        protected override void ObliczDepresjeMaksymalnaWStudni()
        {
            try
            {
                Studnia.DepresjaMaksymalnaWStudni.Wartosc = Stale.StaleProgramu.WspolczynnikDepresjiMaksymalnejStudniSwobodnej * Studnia.WysokoscH.Wartosc;

            }
            catch (Exception e)
            {
                Logger.Log(new Blad() {IdInstancji = Studnia.Id, Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladObliczen + Slownik.Logika.DepresjaMaksymalnaWStudni });
            }
        }

        private void ObliczWysokoscHsMaksymalna()
        {
            try
            {
                ((StudniaSwobodna)Studnia).WysokoscHsMaksymalna.Wartosc = Studnia.WysokoscH.Wartosc - Studnia.DepresjaMaksymalnaWStudni.Wartosc;

            }
            catch (Exception e)
            {
                Logger.Log(new Blad() {IdInstancji = Studnia.Id, Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladObliczen + Slownik.Logika.WysokoscHsMaksymalna });
            }
        }

        private void Oblicz_bmax_temp()
        {
            try
            {
                _bmaxTemp = Math.Pow(Studnia.DlugoscCzesciCzynnejFiltra.Wartosc / _hmaxTemp, 2.0 / 3.0) * Math.Pow(2 - Studnia.DlugoscCzesciCzynnejFiltra.Wartosc / _hmaxTemp, 1.0 / 2.0);

            }
            catch (Exception e)
            {
                Logger.Log(new Blad() {IdInstancji = Studnia.Id, Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladObliczen + Slownik.Logika.PoprawkaForhcheimeraMaksymalnaTemp });
            }
        }

        private void Oblicz_hmax_temp()
        {
            try
            {
                _hmaxTemp = Math.Sqrt(Math.Pow(Studnia.WysokoscH.Wartosc, 2) * (1 - _bmaxTemp) + _bmaxTemp * Math.Pow(((StudniaSwobodna)Studnia).WysokoscHsMaksymalna.Wartosc, 2));
            }
            catch (Exception e)
            {
                Logger.Log(new Blad() {IdInstancji = Studnia.Id, Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladObliczen + Slownik.Logika.WysokoscHMaxTemp });
            }
        }

        protected override void ObliczDepresjeMaksymalnaPozaStudnia()
        {
            try
            {
                Studnia.DepresjaMaksymalnaPozaStudnia.Wartosc = Studnia.WysokoscH.Wartosc - ((StudniaSwobodna)Studnia).WysokoscHMaksymalna.Wartosc;
            }
            catch (Exception e)
            {
                Logger.Log(new Blad() {IdInstancji = Studnia.Id, Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladObliczen + Slownik.Logika.DepresjaMaksymalnaPozaStudnia });
            }
        }

        protected override void ObliczMaksymalnyPromienLejaDepresji()
        {
            try
            {
                Studnia.PromienLejaDepresjiMaksymalny.Wartosc = 2 * Studnia.DepresjaMaksymalnaPozaStudnia.Wartosc * Math.Sqrt(Studnia.WspolczynnikFiltracji.Wartosc * Studnia.WysokoscH.Wartosc);
            }
            catch (Exception e)
            {
                Logger.Log(new Blad() {IdInstancji = Studnia.Id, Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladObliczen + Slownik.Logika.PromienLejaDepresjiMaksymalny });
            }
        }

        protected override void ObliczWydatekMaksymalny()
        {
            try
            {
                Studnia.WydatekMaksymalny.Wartosc = Math.PI * Studnia.WspolczynnikFiltracji.Wartosc * (Math.Pow(Studnia.WysokoscH.Wartosc, 2) - Math.Pow(((StudniaSwobodna)Studnia).WysokoscHMaksymalna.Wartosc, 2)) / Math.Log(Studnia.PromienLejaDepresjiMaksymalny.Wartosc / Studnia.PromienStudni.Wartosc);

            }
            catch (Exception e)
            {
                Logger.Log(new Blad() {IdInstancji = Studnia.Id, Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladObliczen + Slownik.Logika.WydatekMaksymalny });
            }
        }

        protected override void ObliczParametryEksploatacyjne()
        {
            ObliczDepresjeIPromienEksploatacyjne();
            Oblicz_h_eksploatacyjne();
            ObliczPoprawkeForhcheimera();
            Oblicz_hs_eksploatacyjne();
            ObliczDepresjeRzeczywistaWStudni();
            ObliczSprawnoscStudni();
        }

        protected override void ObliczDepresjeIPromienEksploatacyjne()
        {
            try
            {
                _depresjaTemp = WylosujParametrPoczatkowy(Enums.TypyWalidacyjne.Depresja);
                double staraDepresja;
                IPorownywacz porownywacz = new DoublePorownywacz();
                do
                {
                    ObliczPromienEksploatacyjnyTymczasowy();
                    if (JestPozaZakresem(_promienTemp))
                    {
                        throw new WyjatekObliczen(Slownik.Logika.NieprawidlowyPromienTymczasowy);
                    }
                    staraDepresja = _depresjaTemp;
                    ObliczDepresjeEksploatacyjnaTymczasowa();
                    if (JestPozaZakresem(_depresjaTemp))
                    {
                        throw new WyjatekObliczen(Slownik.Logika.NieprawidlowaDepresjaTymczasowa);
                    }

                } while (!porownywacz.SaRowne(staraDepresja, _depresjaTemp, Stale.StaleProgramu.DokladnoscObliczen));

                ObliczPromienEksploatacyjnyTymczasowy();

                Studnia.DepresjaPozaStudnia.Wartosc = _depresjaTemp;
                Studnia.PromienLejaDepresji.Wartosc = _promienTemp;
            }
            catch (Exception e)
            {
                Logger.Log(new Blad() {IdInstancji = Studnia.Id, Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladObliczen + Slownik.Logika.DepresjaPozaStudnia });
            }
        }

        private void ObliczPromienEksploatacyjnyTymczasowy()
        {
            try
            {
                _promienTemp = 2 * _depresjaTemp * Math.Sqrt(Studnia.WspolczynnikFiltracji.Wartosc * Studnia.WysokoscH.Wartosc);

            }
            catch (Exception e)
            {
                Logger.Log(new Blad() {IdInstancji = Studnia.Id, Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladObliczen + Slownik.Logika.PromienTymczasowy });
            }
        }

        private void ObliczDepresjeEksploatacyjnaTymczasowa()
        {
            try
            {
                _depresjaTemp = Studnia.WysokoscH.Wartosc - Math.Sqrt(Math.Pow(Studnia.WysokoscH.Wartosc, 2) - Studnia.Wydatek.Wartosc * Math.Log(_promienTemp / Studnia.PromienStudni.Wartosc) / (Math.PI * Studnia.WspolczynnikFiltracji.Wartosc));

            }
            catch (Exception e)
            {
                Logger.Log(new Blad() {IdInstancji = Studnia.Id, Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladObliczen + Slownik.Logika.DepresjaTymczasowa });
            }
        }

        private void Oblicz_h_eksploatacyjne()
        {
            try
            {
                ((StudniaSwobodna)Studnia).WysokoscHEksploatacyjna.Wartosc = Studnia.WysokoscH.Wartosc - Studnia.DepresjaPozaStudnia.Wartosc;
            }
            catch (Exception e)
            {
                Logger.Log(new Blad() {IdInstancji = Studnia.Id, Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladObliczen + Slownik.Logika.WysokoscHEksploatacyjna });
            }
        }

        protected override void ObliczPoprawkeForhcheimera()
        {
            try
            {
                Studnia.PoprawkaForhcheimera.Wartosc = Math.Pow(Studnia.DlugoscCzesciCzynnejFiltra.Wartosc / ((StudniaSwobodna)Studnia).WysokoscHEksploatacyjna.Wartosc, 2.0 / 3.0) * Math.Sqrt(2 - (Studnia.DlugoscCzesciCzynnejFiltra.Wartosc / ((StudniaSwobodna)Studnia).WysokoscHEksploatacyjna.Wartosc));
            }
            catch (Exception e)
            {
                Logger.Log(new Blad() {IdInstancji = Studnia.Id, Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladObliczen + Slownik.Logika.PoprawkaForhcheimera });
            }
        }

        private void Oblicz_hs_eksploatacyjne()
        {
            try
            {
                ((StudniaSwobodna)Studnia).WysokoscHsEksploatacyjna.Wartosc = Math.Sqrt((Math.Pow(((StudniaSwobodna)Studnia).WysokoscHEksploatacyjna.Wartosc, 2) - Math.Pow(Studnia.WysokoscH.Wartosc, 2) * (1 - Studnia.PoprawkaForhcheimera.Wartosc)) / Studnia.PoprawkaForhcheimera.Wartosc);

            }
            catch (Exception e)
            {
                Logger.Log(new Blad() {IdInstancji = Studnia.Id, Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladObliczen + Slownik.Logika.WysokoscHsEksploatacyjna });
            }
        }

        protected override void ObliczDepresjeRzeczywistaWStudni()
        {
            try
            {
                Studnia.DepresjaWStudni.Wartosc = Studnia.WysokoscH.Wartosc - ((StudniaSwobodna)Studnia).WysokoscHsEksploatacyjna.Wartosc;
            }
            catch (Exception e)
            {
                Logger.Log(new Blad() {IdInstancji = Studnia.Id, Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladObliczen + Slownik.Logika.DepresjaWStudni });
            }
        }
    }
}

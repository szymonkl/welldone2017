﻿using System;

namespace Logika.Wyjatki
{
    public class WyjatekObliczen : Exception
    {
        public WyjatekObliczen(string tekst) : base(tekst) { }
    }
}

﻿using System.Collections.Generic;
using ValidationEngine.ValidationRules;

namespace ValidationEngine.Validators
{
    public class DoubleValidator : ValidatorBase
    {
        public DoubleValidator()
        {
            ValidationRules = new List<IValidationRule>
            {
                new DoubleLessThanMaximumValidationRule(),
                new DoubleGreaterThanMinimumValidationRule()
            };
        }
    }
}

﻿using ValidationEngine.Others;

namespace ValidationEngine.ValidationRules
{
    public class StringNotNullOrEmptyValidationRule : IValidationRule
    {
        public bool IsValid(object target, ValidationInfo info)
        {
            var inputString = (string) target;
            bool isValid = !string.IsNullOrEmpty(inputString);
            if (!isValid)
            {
                info.Message.Add(info.Name + " nie może być pusta!");
            }
            return isValid;
        }
    }
}

﻿using NUnit.Framework;
using Wspolne;

namespace Logika.Testy
{
    [TestFixture]
    public class StudniaNaporowaObliczeniaTesty
    {
        private StudniaNaporowa _studnia1;
        private StudniaNaporowa _studnia2;

        [SetUp]
        public void Start()
        {
            _studnia1 = new StudniaNaporowa
            {
                Wydatek =  {Wartosc = 800},
                PromienStudni = {Wartosc = 11}, //11 cali
                MiazszoscWarstwy = {Wartosc = 12},
                DlugoscCzesciCzynnejFiltra = {Wartosc = 11},
                WysokoscDeltaH =  {Wartosc = 8},
                WspolczynnikFiltracji = {Wartosc = 25},
                SposobDzialania = Enums.SposobyDzialania.Ciagly
            };

            _studnia2 = new StudniaNaporowa
            {
                Wydatek = { Wartosc = 800 },
                SrednicaStudni = { Wartosc = 22 }, //22 cale
                MiazszoscWarstwy = { Wartosc = 12 },
                DlugoscCzesciCzynnejFiltra = { Wartosc = 11 },
                WysokoscDeltaH = { Wartosc = 8 },
                WspolczynnikFiltracji = { Wartosc = 25 },
                SposobDzialania = Enums.SposobyDzialania.Ciagly
            };

            StudniaObliczenia obliczenia1 = new StudniaNaporowaObliczenia(_studnia1);
            obliczenia1.Wykonaj();
            StudniaObliczenia obliczenia2 = new StudniaNaporowaObliczenia(_studnia2);
            obliczenia2.Wykonaj();

        }
        [Test]
        public void StudniaNaporowa_PredkoscDopuszczalna_JestOK()
        {
            Assert.That(_studnia1.PredkoscDopuszczalna.Wartosc, Is.EqualTo(49.00).Within(0.01));
            Assert.That(_studnia2.PredkoscDopuszczalna.Wartosc, Is.EqualTo(49.00).Within(0.01));
        }
        [Test]
        public void StudniaNaporowa_WydatekDopuszczalny_JestOK()
        {
            Assert.That(_studnia1.WydatekDopuszczalny.Wartosc, Is.EqualTo(946.22).Within(0.01));
            Assert.That(_studnia2.WydatekDopuszczalny.Wartosc, Is.EqualTo(946.22).Within(0.01));
        }
        [Test]
        public void StudniaNaporowa_WysokoscH_JestOK()
        {
            Assert.That(_studnia1.WysokoscH.Wartosc, Is.EqualTo(20).Within(0.01));
            Assert.That(_studnia2.WysokoscH.Wartosc, Is.EqualTo(20).Within(0.01));
        }

        [Test]
        public void StudniaNaporowa_PromienMaksymalny_JestOK()
        {
            Assert.That(_studnia1.PromienLejaDepresjiMaksymalny.Wartosc, Is.EqualTo(235.72).Within(0.01));
            Assert.That(_studnia2.PromienLejaDepresjiMaksymalny.Wartosc, Is.EqualTo(235.72).Within(0.01));
        }
        [Test]
        public void StudniaNaporowa_DepresjaWStudniMaksymalna_JestOK()
        {
            Assert.That(_studnia1.DepresjaMaksymalnaWStudni.Wartosc, Is.EqualTo(4.80).Within(0.01));
            Assert.That(_studnia2.DepresjaMaksymalnaWStudni.Wartosc, Is.EqualTo(4.80).Within(0.01));
        }
        [Test]
        public void StudniaNaporowa_DepresjaPozaStudniaMaksymalna_JestOK()
        {
            Assert.That(_studnia1.DepresjaMaksymalnaPozaStudnia.Wartosc, Is.EqualTo(4.71).Within(0.01));
            Assert.That(_studnia2.DepresjaMaksymalnaPozaStudnia.Wartosc, Is.EqualTo(4.71).Within(0.01));
        }
        [Test]
        public void StudniaNaporowa_WydatekMaksymalny_JestOK()
        {
            Assert.That(_studnia1.WydatekMaksymalny.Wartosc, Is.EqualTo(1318.91).Within(0.01));
            Assert.That(_studnia2.WydatekMaksymalny.Wartosc, Is.EqualTo(1318.91).Within(0.01));
        }
        [Test]
        public void StudniaNaporowa_PromienEksploatacyjny_JestOK()
        {
            Assert.That(_studnia1.PromienLejaDepresji.Wartosc, Is.EqualTo(130.42).Within(0.01));
            Assert.That(_studnia2.PromienLejaDepresji.Wartosc, Is.EqualTo(130.42).Within(0.01));
        }
        [Test]
        public void StudniaNaporowa_DepresjaWStudniEksploatacyjna_JestOK()
        {
            Assert.That(_studnia1.DepresjaWStudni.Wartosc, Is.EqualTo(2.66).Within(0.01));
            Assert.That(_studnia2.DepresjaWStudni.Wartosc, Is.EqualTo(2.66).Within(0.01));
        }
        [Test]
        public void StudniaNaporowa_DepresjaPozaStudniaEksploatacyjna_JestOK()
        {
            Assert.That(_studnia1.DepresjaPozaStudnia.Wartosc, Is.EqualTo(2.61).Within(0.01));
            Assert.That(_studnia2.DepresjaPozaStudnia.Wartosc, Is.EqualTo(2.61).Within(0.01));
        }
        [Test]
        public void StudniaNaporowa_WydatekEksploatacyjny_JestOK()
        {
            Assert.That(_studnia1.Wydatek.Wartosc, Is.EqualTo(800.00).Within(0.01));
            Assert.That(_studnia2.Wydatek.Wartosc, Is.EqualTo(800.00).Within(0.01));
        }
        [Test]
        public void StudniaNaporowa_PoprawkaForhcheimera_JestOK()
        {
            Assert.That(_studnia1.PoprawkaForhcheimera.Wartosc, Is.EqualTo(0.98).Within(0.01));
            Assert.That(_studnia2.PoprawkaForhcheimera.Wartosc, Is.EqualTo(0.98).Within(0.01));
        }
        [Test]
        public void StudniaNaporowa_SprawnoscStudni_JestOK()
        {
            Assert.That(_studnia1.Sprawnosc.Wartosc, Is.EqualTo(98.0).Within(1));
            Assert.That(_studnia2.Sprawnosc.Wartosc, Is.EqualTo(98.0).Within(1));
        }



    }
}

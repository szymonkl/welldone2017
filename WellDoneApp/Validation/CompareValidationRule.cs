﻿using System.Globalization;
using System.Windows.Controls;
using BusinessLayer;
using Logika;
using Logowanie;

namespace WellDoneApp.Validation
{
    public class CompareValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            ValidationResult result = ValidationResult.ValidResult;
            Parametr parametr = value as Parametr;
            if (parametr != null)
            {
                ErrorManager manager = ErrorManager.StworzSingleton();
                Parametr powiazanyParametr = parametr.PowiazanyParametr;
                
                if (powiazanyParametr != null)
                {
                    string wiadomoscBledu;
                    if (parametr.WalidacjaInfo.JestWiekszeOdPowiazanego)
                    {
                        wiadomoscBledu = Slownik.Logika.WartoscParametru + parametr.NazwaWyswietlana +
                                         Slownik.Logika.WiekszaOd + powiazanyParametr.NazwaWyswietlana;
                        if (parametr.Wartosc > powiazanyParametr.Wartosc)
                        {
                            result = ValidationResult.ValidResult;
                            manager.RemoveError(new Blad { Wiadomosc = wiadomoscBledu});
                        }
                        else
                        {
                            result = new ValidationResult(false, wiadomoscBledu);
                            manager.AddError(new Blad  { Wiadomosc = wiadomoscBledu});
                        }

                    }
                    else
                    {
                        wiadomoscBledu = Slownik.Logika.WartoscParametru + parametr.NazwaWyswietlana +
                                         Slownik.Logika.MniejszaOd + powiazanyParametr.NazwaWyswietlana;
                        if (parametr.Wartosc < powiazanyParametr.Wartosc)
                        {
                            result = ValidationResult.ValidResult;
                            manager.RemoveError(new Blad { Wiadomosc = wiadomoscBledu});
                        }
                        else
                        {
                            
                            result = new ValidationResult(false, wiadomoscBledu);
                            manager.AddError(new Blad { Wiadomosc = wiadomoscBledu});
                        }

                    }

                }
            }
            return result;
        }
    }
}

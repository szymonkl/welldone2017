﻿using System.Collections.Generic;
using Wspolne;

namespace Stale
{
    public static class StaleStudni
    {
        public static Dictionary<Enums.SposobyDzialania, double> PredkoscDopuszczalna { get; set; }

        static StaleStudni()
        {
            PredkoscDopuszczalna = new Dictionary<Enums.SposobyDzialania, double>
            {
                {Enums.SposobyDzialania.Ciagly, 9.8},
                {Enums.SposobyDzialania.Awaryjny, 19.6},
                {Enums.SposobyDzialania.Odwadnianie, 65}
            };
        }



    }
}

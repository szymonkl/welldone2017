﻿using System.Collections.Generic;
using ValidationEngine.Others;
using ValidationEngine.ValidationRules;

namespace ValidationEngine.Validators
{
    public abstract class ValidatorBase
    {
        public List<IValidationRule> ValidationRules { get; set; }

        public virtual bool Validate(object target, ValidationInfo info)
        {
            var isValid = true;
            foreach (var validationRule in ValidationRules)
            {
                if (!validationRule.IsValid(target, info))
                {
                    isValid = false;
                }
            }
            return isValid;
        }

    }
}

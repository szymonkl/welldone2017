﻿using System.Collections.Generic;
using System.Linq;
using Slownik;

namespace Wspolne
{
    public class Enums
    {
        public enum Jednostki
        {
            [Nazwa("[-]")]
            Brak,
            [Nazwa("[%]")]
            Procent,
            #region Czas
            [Nazwa("[s]")]
            Sekunda,
            [Nazwa("[min]")]
            Minuta,
            [Nazwa("[h]")]
            Godzina,
            [Nazwa("[d]")]
            Doba,
            #endregion
            #region Dlugosc
            [Nazwa("[cm]")]
            [TypJednostki(TypyParametru.Dlugosc)]
            Centymetr,
            [Nazwa("[dm]")]
            [TypJednostki(TypyParametru.Dlugosc)]
            Decymetr,
            [Nazwa("[m]")]
            [TypJednostki(TypyParametru.Dlugosc)]
            Metr,
            [Nazwa("[km]")]
            [TypJednostki(TypyParametru.Dlugosc)]
            Kilometr,
            [Nazwa("[inch]")]
            [TypJednostki(TypyParametru.Dlugosc)]
            Cal,
            #endregion
            #region Predkosc
            [Nazwa("[m/s]")]
            [TypJednostki(TypyParametru.Predkosc)]
            MetrSekunda,
            [Nazwa("[m/h]")]
            [TypJednostki(TypyParametru.Predkosc)]
            MetrGodzina,
            [Nazwa("[m/d]")]
            [TypJednostki(TypyParametru.Predkosc)]
            MetrDoba,
            [Nazwa("[inch/s]")]
            [TypJednostki(TypyParametru.Predkosc)]
            CalSekunda,

            #endregion
            #region Wydatek
            [Nazwa("[m3/d]")]
            [TypJednostki(TypyParametru.Wydatek)]
            Metr3Doba,
            [Nazwa("[m3/h]")]
            [TypJednostki(TypyParametru.Wydatek)]
            Metr3Godzina,
            [Nazwa("[l/s]")]
            [TypJednostki(TypyParametru.Wydatek)]
            LitrSekunda,
            [Nazwa("[gal/d]")]
            [TypJednostki(TypyParametru.Wydatek)]
            GalonDoba,
            [Nazwa("[gal/h]")]
            [TypJednostki(TypyParametru.Wydatek)]
            GalonGodzina
            #endregion
        }

        public static List<Jednostki> JednostkiDlugosci => PobierzJednostkiWgTypu(TypyParametru.Dlugosc);

        public static List<Jednostki> JednostkiPredkosci => PobierzJednostkiWgTypu(TypyParametru.Predkosc);

        public static List<Jednostki> JednostkiWydatku => PobierzJednostkiWgTypu(TypyParametru.Wydatek);
       

        private static List<Jednostki> PobierzJednostkiWgTypu(Enums.TypyParametru typ)
        {
            var enumValues = typeof(Jednostki)
                .GetFields()
                .Select(x => new
                {
                    att = x.GetCustomAttributes(false)
                        .OfType<TypJednostkiAttribute>()
                        .FirstOrDefault(),
                    member = x
                })
                .Where(x => x.att != null && x.att.TypParametru == typ)
                .Select(x => (Jednostki)x.member.GetValue(null))
                .ToList();
            return enumValues;
        }

        public enum TypyParametru
        {
            Brak,
            Czas,
            Dlugosc,
            Predkosc,
            Wydatek
        }

        public enum SposobyDzialania
        {
            Ciagly,
            Awaryjny,
            Odwadnianie
        }

        public enum TypyWalidacyjne
        {
            Wydatek,
            Depresja,
            PromienLeja,
            PromienStudni,
            WspolczynnikFiltracji,
            Miazszosc,
            DlugoscFiltra,
            PoprawkaForhcheimera,
            Sprawnosc,
            PredkoscWody
        }

        public enum TypyBledow
        {
            BladWalidacji,
            Ostrzezenie,
            BladKrytyczny,
        }

    }
}

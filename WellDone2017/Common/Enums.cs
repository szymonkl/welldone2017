﻿using Wspolne;

namespace WellDone2017.Common
{
    public class Enums
    {
        public enum JednostkiWydatku
        {
            [Nazwa("[m3/d]")]
            Metr3Doba,
            [Nazwa("[m3/h]")]
            Metr3Godzina,
            [Nazwa("[l/s]")]
            LitrSekunda,
            [Nazwa("[gal/d]")]
            GalonDoba,
            [Nazwa("[gal/h]")]
            GalonGodzina
        }

        public enum JednostkiPredkosci
        {
            [Nazwa("[m/s]")]
            MetrSekunda,
            [Nazwa("[m/h]")]
            MetrGodzina,
            [Nazwa("[m/d]")]
            MetrDoba,
            [Nazwa("[inch/s]")]
            CalSekunda,
        }

        public enum JednostkiDlugosci
        {
            [Nazwa("[cm]")]
            Centymetr,
            [Nazwa("[dm]")]
            Decymetr,
            [Nazwa("[m]")]
            Metr,
            [Nazwa("[km]")]
            Kilometr,
            [Nazwa("[inch]")]
            Cal,
        }
    }
}

﻿using System;
using BusinessLayer;
using Stale;
using Wspolne;
using Logowanie;

namespace Logika
{
    public abstract class StudniaObliczenia
    {
        protected Studnia Studnia;
        protected readonly Predicate<double> JestPozaZakresem = w => double.IsInfinity(w) || double.IsNaN(w);
        protected Logger Logger;

        protected StudniaObliczenia(Studnia studnia)
        {
            Studnia = studnia;
            Logger = Logger.StworzSingleton();
        }

        protected virtual double WylosujParametrPoczatkowy(Enums.TypyWalidacyjne typ)
        {
            try
            {
                var minimum = ParametryWalidacyjne.Minimalne[typ];
                var maksimum = ParametryWalidacyjne.Maksymalne[typ];
                var random = new Random();
                var wylosowanaliczba = random.NextDouble() * (maksimum - minimum) + minimum;
                return wylosowanaliczba;
            }
            catch (Exception e)
            {
               Logger.Log(new Blad(){Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladLosowaniaParametrow});
               return 0;
            }
        }

        public void Wykonaj()
        {
            KonwertujDoJednostekDocelowych();
            ObliczPromienStudni();
            ObliczParametryDopuszczalne();
            ObliczParametryMaksymalne();
            ObliczParametryEksploatacyjne();
            KonwertujDoJednostekAktualnych();
        }
        
        private void KonwertujDoJednostekDocelowych()
        {
            try
            {
                var konwerter = new KonwerterJednostek();
                var ustawienia = Ustawienia.StworzSingleton();
                Studnia.ListaParametrow.ForEach(parametr => konwerter.Konwertuj(parametr, ustawienia.Docelowe.JednostkiParametrow[parametr.Nazwa]));
            }
            catch (Exception e)
            {
                Logger.Log(new Blad(){Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladKonwersjiParametrowDocelowych});   
            }
        }

        protected void ObliczPromienStudni()
        {
            try
            {
                 if (Studnia.SrednicaStudni.Wartosc == 0)
                {
                    Studnia.SrednicaStudni.Wartosc = Studnia.PromienStudni.Wartosc * 2;
                }
                Studnia.PromienStudni.Wartosc = Studnia.SrednicaStudni.Wartosc / 2;
            }
            catch (Exception e)
            {
                Logger.Log(new Blad() { Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladObliczen + Slownik.Logika.PromienStudni});
            }
        }

        protected void ObliczParametryDopuszczalne()
        {
            ObliczPredkoscDopuszczalna();
            ObliczWydatekDopuszczalny();
        }

        private void ObliczPredkoscDopuszczalna()
        {
            try
            {
                Studnia.PredkoscDopuszczalna.Wartosc = StaleStudni.PredkoscDopuszczalna[Studnia.SposobDzialania] *
                                                           Math.Sqrt(Studnia.WspolczynnikFiltracji.Wartosc);
            }
            catch (Exception e)
            {
                Logger.Log(new Blad(){ Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladObliczen + Slownik.Logika.PredkoscDopuszczalna});   
            }
        }

        private void ObliczWydatekDopuszczalny()
        {
            try
            {
                Studnia.WydatekDopuszczalny.Wartosc = Studnia.PredkoscDopuszczalna.Wartosc * 2 * Studnia.PromienStudni.Wartosc * Studnia.DlugoscCzesciCzynnejFiltra.Wartosc * Math.PI;
            }
            catch (Exception e)
            {
                Logger.Log(new Blad() { Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladObliczen + Slownik.Logika.WydatekDopuszczalny});   
            }
        }

        protected abstract void ObliczParametryMaksymalne();

        protected abstract void ObliczDepresjeMaksymalnaPozaStudnia();

        protected abstract void ObliczDepresjeMaksymalnaWStudni();

        protected abstract void ObliczMaksymalnyPromienLejaDepresji();

        protected abstract void ObliczWydatekMaksymalny();

        protected abstract void ObliczParametryEksploatacyjne();

        protected abstract void ObliczDepresjeIPromienEksploatacyjne();

        protected abstract void ObliczPoprawkeForhcheimera();

        protected virtual void ObliczDepresjeRzeczywistaWStudni()
        {
            try
            {
                Studnia.DepresjaWStudni.Wartosc = Studnia.DepresjaPozaStudnia.Wartosc / Studnia.PoprawkaForhcheimera.Wartosc;
            }
            catch (Exception e)
            {
                Logger.Log(new Blad() { Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladObliczen + Slownik.Logika.DepresjaWStudni});
            }
        }

        protected void ObliczSprawnoscStudni()
        {
            try
            {
                Studnia.Sprawnosc.Wartosc = 100 * Studnia.DepresjaPozaStudnia.Wartosc / Studnia.DepresjaWStudni.Wartosc;
            }
            catch (Exception e)
            {
                Logger.Log(new Blad() { Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladObliczen + Slownik.Logika.Sprawnosc});   
            }
        }

        private void KonwertujDoJednostekAktualnych()
        {
            try
            {
                var konwerter = new KonwerterJednostek();
                var ustawienia = Ustawienia.StworzSingleton();
                Studnia.ListaParametrow.ForEach(parametr => konwerter.Konwertuj(parametr, ustawienia.Aktualne.JednostkiParametrow[parametr.Nazwa]));
            }
            catch (Exception e)
            {
                Logger.Log(new Blad(){ Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladKonwersjiParametrowAktualnych});  
            }
        }



    }
}

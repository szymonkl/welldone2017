﻿using Wspolne;
using NUnit.Framework;

namespace Logika.Testy
{
    [TestFixture]
    public class StudniaNaporowaTesty
    {
        [Test]
        public void StudniaNaporowa_InicjalizacjaDanych_PoprawneWartosciPredkosciDopuszczalnej()
        {
            Studnia studnia = new StudniaNaporowa();
            Assert.AreEqual("PredkoscDopuszczalna", studnia.PredkoscDopuszczalna.Nazwa);
            Assert.AreEqual(Enums.TypyParametru.Predkosc, studnia.PredkoscDopuszczalna.Typ);
            Assert.AreEqual(10000, studnia.PredkoscDopuszczalna.Maksimum);
            Assert.AreEqual(0.01, studnia.PredkoscDopuszczalna.Minimum);
            Assert.AreEqual(Enums.Jednostki.MetrDoba, studnia.PredkoscDopuszczalna.Jednostka.Nazwa);
        }

        [Test]
        public void StudniaNaporowa_InicjalizacjaDanych_PoprawneWartosciWydatku()
        {
            Studnia studnia = new StudniaNaporowa();
            Assert.AreEqual("Wydatek", studnia.Wydatek.Nazwa);
            Assert.AreEqual(Enums.TypyParametru.Wydatek, studnia.Wydatek.Typ);
            Assert.AreEqual(100000, studnia.Wydatek.Maksimum);
            Assert.AreEqual(0.1, studnia.Wydatek.Minimum);
            Assert.AreEqual(Enums.Jednostki.Metr3Doba, studnia.Wydatek.Jednostka.Nazwa);
        }

        [Test]
        public void StudniaNaporowa_InicjalizacjaDanych_PoprawneWartosciDepresji()
        {
            Studnia studnia = new StudniaNaporowa();
            Assert.AreEqual("DepresjaPozaStudnia", studnia.DepresjaPozaStudnia.Nazwa);
            Assert.AreEqual(Enums.TypyParametru.Dlugosc, studnia.DepresjaPozaStudnia.Typ);
            Assert.AreEqual(100, studnia.DepresjaPozaStudnia.Maksimum);
            Assert.AreEqual(0.01, studnia.DepresjaPozaStudnia.Minimum);
            Assert.AreEqual(Enums.Jednostki.Metr, studnia.DepresjaPozaStudnia.Jednostka.Nazwa);
        }
        [Test]
        public void StudniaNaporowa_InicjalizacjaDanych_PoprawneWartosciWysokosciDeltaH()
        {
            StudniaNaporowa studnia = new StudniaNaporowa();
            Assert.AreEqual("WysokoscDeltaH", studnia.WysokoscDeltaH.Nazwa);
            Assert.AreEqual(Enums.TypyParametru.Dlugosc, studnia.DepresjaPozaStudnia.Typ);
            Assert.AreEqual(1000, studnia.WysokoscDeltaH.Maksimum);
            Assert.AreEqual(0.01, studnia.WysokoscDeltaH.Minimum);
            Assert.AreEqual(Enums.Jednostki.Metr, studnia.WysokoscDeltaH.Jednostka.Nazwa);
        }

    }
}

﻿using NUnit.Framework;
using Wspolne;

namespace Logika.Testy
{
    public class StudniaSwobodnaTesty
    {
        [Test]
        public void StudniaSwobodna_InicjalizacjaDanych_PoprawneWartosciWydatkuDopuszczalnego()
        {
            Studnia studnia = new StudniaSwobodna();
            Assert.AreEqual("WydatekDopuszczalny", studnia.WydatekDopuszczalny.Nazwa);
            Assert.AreEqual(Enums.TypyParametru.Wydatek, studnia.WydatekDopuszczalny.Typ);
            Assert.AreEqual(100000, studnia.WydatekDopuszczalny.Maksimum);
            Assert.AreEqual(0.1, studnia.WydatekDopuszczalny.Minimum);
            Assert.AreEqual(Enums.Jednostki.Metr3Doba, studnia.WydatekDopuszczalny.Jednostka.Nazwa);
        }

        [Test]
        public void StudniaSwobodna_InicjalizacjaDanych_PoprawneWartosciWspolczynnikaFiltracji()
        {
            Studnia studnia = new StudniaSwobodna();
            Assert.AreEqual("WspolczynnikFiltracji", studnia.WspolczynnikFiltracji.Nazwa);
            Assert.AreEqual(Enums.TypyParametru.Predkosc, studnia.WspolczynnikFiltracji.Typ);
            Assert.AreEqual(1000, studnia.WspolczynnikFiltracji.Maksimum);
            Assert.AreEqual(0.0000000000001, studnia.WspolczynnikFiltracji.Minimum);
            Assert.AreEqual(Enums.Jednostki.MetrDoba, studnia.WspolczynnikFiltracji.Jednostka.Nazwa);
        }

        [Test]
        public void StudniaSwobodna_InicjalizacjaDanych_PoprawneWartosciDepresji()
        {
            Studnia studnia = new StudniaSwobodna();
            Assert.AreEqual("DepresjaPozaStudnia", studnia.DepresjaPozaStudnia.Nazwa);
            Assert.AreEqual(Enums.TypyParametru.Dlugosc, studnia.DepresjaPozaStudnia.Typ);
            Assert.AreEqual(100, studnia.DepresjaPozaStudnia.Maksimum);
            Assert.AreEqual(0.01, studnia.DepresjaPozaStudnia.Minimum);
            Assert.AreEqual(Enums.Jednostki.Metr, studnia.DepresjaPozaStudnia.Jednostka.Nazwa);
        }
        [Test]
        public void StudniaSwobodna_InicjalizacjaDanych_PoprawneWartosciWysokosciHMaksymalnej()
        {
            StudniaSwobodna studnia = new StudniaSwobodna();
            Assert.AreEqual("WysokoscHMaksymalna", studnia.WysokoscHMaksymalna.Nazwa);
            Assert.AreEqual(Enums.TypyParametru.Dlugosc, studnia.WysokoscHMaksymalna.Typ);
            Assert.AreEqual(1000, studnia.WysokoscHMaksymalna.Maksimum);
            Assert.AreEqual(0.01, studnia.WysokoscHMaksymalna.Minimum);
            Assert.AreEqual(Enums.Jednostki.Metr, studnia.WysokoscHMaksymalna.Jednostka.Nazwa);
        }
    }
}

﻿using System;
using System.Windows.Input;

namespace WellDoneApp.Tabs
{
    public interface ITab
    {
        string Name { get; set; }
        string Title { get; set; }
        ICommand CloseCommand { get; set; }
        event EventHandler CloseRequested;
    }
}

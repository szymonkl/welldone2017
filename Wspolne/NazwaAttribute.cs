﻿using System;

namespace Wspolne
{
    
    public class NazwaAttribute : Attribute
    {
        public NazwaAttribute(string tekst)
        {
            Tekst = tekst;
        }

        public string Tekst { get; set; }
    }
}

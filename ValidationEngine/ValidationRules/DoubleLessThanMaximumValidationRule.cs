﻿using ValidationEngine.Others;

namespace ValidationEngine.ValidationRules
{
    public class DoubleLessThanMaximumValidationRule : IValidationRule
    {
        public bool IsValid(object target, ValidationInfo info)
        {
            var value = (double) target;
            bool isValid = value < ValidationParameters.MaxValues[info.TypWalidacyjny];
            if (!isValid)
            {
                info.Message.Add(info.Name + " jest za duża!");
            }
            return isValid;
        }
    }
}

﻿using ValidationEngine.Others;

namespace ValidationEngine.ValidationRules
{
    public interface IValidationRule
    {
        bool IsValid(object target, ValidationInfo info);
    }
}

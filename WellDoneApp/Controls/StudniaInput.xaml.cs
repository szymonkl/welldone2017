﻿using System;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using WellDone2017.Controls;
using WellDoneApp.Validation;
using WellDoneApp.ViewModels;

namespace WellDoneApp.Controls
{
    /// <summary>
    /// Interaction logic for StudniaInput.xaml
    /// </summary>
    public partial class StudniaInput : UserControl
    {
        public StudniaInput()
        {
            InitializeComponent();
            inDlugoscFiltra.tbWartosc.LostFocus += InDlugoscFiltraOnLostFocus;
            inMiazszoscWarstwy.tbWartosc.LostFocus += InMiazszoscWarstwyOnLostFocus;
        }

        private void InMiazszoscWarstwyOnLostFocus(object sender, RoutedEventArgs e)
        {
            inDlugoscFiltra.GetBindingExpression(Input.ParametrProperty)?.UpdateSource();
        }

        private void InDlugoscFiltraOnLostFocus(object sender, RoutedEventArgs routedEventArgs)
        {
            inMiazszoscWarstwy.GetBindingExpression(Input.ParametrProperty)?.UpdateSource();
        }
    }
}

﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using BusinessLayer.Annotations;
using Wspolne;

namespace Logika
{
    public class Parametr : INotifyPropertyChanged
    {
        private double _wartosc;
        private string _nazwa;
        private double _minimum;
        private double _maksimum;
        private string _nazwaWyswietlana;

        public string Nazwa
        {
            get { return _nazwa; }
            set
            {
                if (value == _nazwa) return;
                _nazwa = value;
                _nazwaWyswietlana = Slownik.Logika.ResourceManager.GetString(_nazwa);
                OnPropertyChanged();
            }
        }

        public string NazwaWyswietlana
        {
            get { return _nazwaWyswietlana; }
            set
            {
                if (value == _nazwaWyswietlana) return;
                _nazwaWyswietlana = value;
                OnPropertyChanged();
            }
        }

        public double Wartosc
        {
            get { return _wartosc; }
            set
            {
                if (value.Equals(_wartosc)) return;
                _wartosc = value;
                OnPropertyChanged();
            }
        }

        public double Minimum
        {
            get { return _minimum; }
            set
            {
                if (value.Equals(_minimum)) return;
                _minimum = value;
                OnPropertyChanged();
            }
        }

        public double Maksimum
        {
            get { return _maksimum; }
            set
            {
                if (value.Equals(_maksimum)) return;
                _maksimum = value;
                OnPropertyChanged();
            }
        }

        public Jednostka Jednostka { get; set; }
        public Enums.TypyParametru Typ { get; set; }
        public Parametr PowiazanyParametr { get; set; }
        public WalidacjaInfo WalidacjaInfo { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
        
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

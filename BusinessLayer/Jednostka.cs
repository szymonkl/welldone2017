﻿
using Stale;
using Wspolne;

namespace Logika
{
    public class Jednostka
    {
        public Enums.Jednostki Nazwa { get; set; }
        public string Opis { get; set; }
        public double Wspolczynnik { get; set; }

        public Jednostka(Enums.Jednostki nazwa)
        {
            Nazwa = nazwa;
            Opis = PobierzWartoscAtrybutu(Nazwa);
            Wspolczynnik = PobierzWspolczynnik(Nazwa);

        }

        private static string PobierzWartoscAtrybutu(Enums.Jednostki jednostka)
        {
            var typ = typeof(Enums.Jednostki);
            var info = typ.GetMember(jednostka.ToString());
            var atrybuty = info[0].GetCustomAttributes(typeof(NazwaAttribute), false);
            if (atrybuty.Length>0)
            {
                return ((NazwaAttribute)atrybuty[0]).Tekst;
            }

            return string.Empty;
        }

        private static double PobierzWspolczynnik(Enums.Jednostki jednostka)
        {
            return StaleJednostek.Wszystkie[jednostka];
        }


    }
}

﻿using System;
using Logika.Wyjatki;
using Logowanie;
using Wspolne;

namespace Logika
{
    public class StudniaNaporowaObliczenia : StudniaObliczenia
    {
        private double _promienLejaTymczasowy;
        private double _depresjaTymczasowa;

        public StudniaNaporowaObliczenia(Studnia studnia) 
            : base(studnia)
        {   }

        protected override void ObliczParametryMaksymalne()
        {
            ObliczWysokoscH();
            ObliczDepresjeMaksymalnaWStudni();
            ObliczPoprawkeForhcheimera();
            ObliczDepresjeMaksymalnaPozaStudnia();
            ObliczMaksymalnyPromienLejaDepresji();
            ObliczWydatekMaksymalny();
        }
        private void ObliczWysokoscH()
        {
            try
            {
                Studnia.WysokoscH.Wartosc = Studnia.MiazszoscWarstwy.Wartosc +
                                                ((StudniaNaporowa)Studnia).WysokoscDeltaH.Wartosc;
            }
            catch (Exception e)
            {
                Logger.Log(new Blad() {IdInstancji = Studnia.Id, Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladObliczen + Slownik.Logika.WysokoscH});
            }
        }

        protected override void ObliczDepresjeMaksymalnaWStudni()
        {
            try
            {
                Studnia.DepresjaMaksymalnaWStudni.Wartosc =
                        Stale.StaleProgramu.WspolczynnikWysokosciMaksymalnejStudniNaporowej *
                        ((StudniaNaporowa)Studnia).WysokoscDeltaH.Wartosc;
            }
            catch (Exception e)
            {
                Logger.Log(new Blad() {IdInstancji = Studnia.Id, Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladObliczen + Slownik.Logika.DepresjaMaksymalnaWStudni });
            }
        }

        protected override void ObliczPoprawkeForhcheimera()
        {
            try
            {
                Studnia.PoprawkaForhcheimera.Wartosc =
                        Math.Pow(Studnia.DlugoscCzesciCzynnejFiltra.Wartosc / Studnia.MiazszoscWarstwy.Wartosc, 2.0 / 3.0) *
                        Math.Sqrt(2 - Studnia.DlugoscCzesciCzynnejFiltra.Wartosc / Studnia.MiazszoscWarstwy.Wartosc);
            }
            catch (Exception e)
            {
                Logger.Log(new Blad() {IdInstancji = Studnia.Id, Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladObliczen + Slownik.Logika.PoprawkaForhcheimera });
            }
        }

        protected override void ObliczDepresjeMaksymalnaPozaStudnia()
        {
            try
            {
                Studnia.DepresjaMaksymalnaPozaStudnia.Wartosc =
                       Studnia.DepresjaMaksymalnaWStudni.Wartosc * Studnia.PoprawkaForhcheimera.Wartosc;
            }
            catch (Exception e)
            {
                Logger.Log(new Blad() {IdInstancji = Studnia.Id, Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladObliczen + Slownik.Logika.DepresjaMaksymalnaPozaStudnia });
            }
        }

        protected override void ObliczMaksymalnyPromienLejaDepresji()
        {
            try
            {
                Studnia.PromienLejaDepresjiMaksymalny.Wartosc = 10 * Studnia.DepresjaMaksymalnaPozaStudnia.Wartosc * Math.Sqrt(Studnia.WspolczynnikFiltracji.Wartosc);
            }
            catch (Exception e)
            {
                Logger.Log(new Blad() {IdInstancji = Studnia.Id, Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladObliczen + Slownik.Logika.PromienLejaDepresjiMaksymalny });
            }
        }

        protected override void ObliczWydatekMaksymalny()
        {
            try
            {
                Studnia.WydatekMaksymalny.Wartosc = 2 * Math.PI * Studnia.WspolczynnikFiltracji.Wartosc * Studnia.MiazszoscWarstwy.Wartosc * Studnia.DepresjaMaksymalnaPozaStudnia.Wartosc /
                                                       Math.Log(Studnia.PromienLejaDepresjiMaksymalny.Wartosc / Studnia.PromienStudni.Wartosc);
            }
            catch (Exception e)
            {
                Logger.Log(new Blad() {IdInstancji = Studnia.Id, Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladObliczen + Slownik.Logika.WydatekMaksymalny });
            }
        }

        protected override void ObliczParametryEksploatacyjne()
        {
            ObliczPoprawkeForhcheimera();
            ObliczDepresjeIPromienEksploatacyjne();
            ObliczDepresjeRzeczywistaWStudni();
            ObliczSprawnoscStudni();
        }

        protected override void ObliczDepresjeIPromienEksploatacyjne()
        {
            try
            {
                _depresjaTymczasowa = WylosujParametrPoczatkowy(Enums.TypyWalidacyjne.Depresja);
                double staraDepresja;
                IPorownywacz porownywacz = new DoublePorownywacz();
                do
                {
                    ObliczPromienTymczasowy();
                    if (JestPozaZakresem(_promienLejaTymczasowy))
                    {
                        throw new WyjatekObliczen(Slownik.Logika.NieprawidlowyPromienTymczasowy);
                    }
                    staraDepresja = _depresjaTymczasowa;
                    ObliczDepresjeTymczasowa();
                    if (JestPozaZakresem(_depresjaTymczasowa))
                    {
                        throw new WyjatekObliczen(Slownik.Logika.NieprawidlowaDepresjaTymczasowa);
                    }

                } while (!porownywacz.SaRowne(staraDepresja, _depresjaTymczasowa, Stale.StaleProgramu.DokladnoscObliczen));
                ObliczPromienTymczasowy();
                Studnia.DepresjaPozaStudnia.Wartosc = _depresjaTymczasowa;
                Studnia.PromienLejaDepresji.Wartosc = _promienLejaTymczasowy;
            }
            catch (Exception e)
            {
                Logger.Log(new Blad() {IdInstancji = Studnia.Id, Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladObliczen + Slownik.Logika.DepresjaPozaStudnia });
            }
        }

        private void ObliczPromienTymczasowy()
        {
            try
            {
                _promienLejaTymczasowy = 10 * _depresjaTymczasowa * Math.Sqrt(Studnia.WspolczynnikFiltracji.Wartosc);
            }
            catch (Exception e)
            {
                Logger.Log(new Blad() {IdInstancji = Studnia.Id, Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladObliczen + Slownik.Logika.PromienTymczasowy });
            }
        }

        private void ObliczDepresjeTymczasowa()
        {
            try
            {
                _depresjaTymczasowa = Studnia.Wydatek.Wartosc * Math.Log(_promienLejaTymczasowy / Studnia.PromienStudni.Wartosc) / (2 * Math.PI * Studnia.WspolczynnikFiltracji.Wartosc * Studnia.MiazszoscWarstwy.Wartosc);
            }
            catch (Exception e)
            {
                Logger.Log(new Blad() {IdInstancji = Studnia.Id, Typ = Enums.TypyBledow.BladKrytyczny, Wyjatek = e, Wiadomosc = Slownik.Logika.BladObliczen + Slownik.Logika.DepresjaTymczasowa });
            }
        }
    }

    

}

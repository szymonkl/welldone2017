﻿using Logika;
using Wspolne;

namespace BusinessLayer
{
    public class Ustawienia
    {
        private static Ustawienia _instancja;
        private static readonly object Blokada = new object();
        public UstawieniaJednostek Domyslne { get; }
        public UstawieniaJednostek Docelowe { get; }
        public UstawieniaJednostek Aktualne { get; set; }

        public Ustawienia()
        {
            Domyslne = new UstawieniaJednostek();
            Docelowe = new UstawieniaJednostek
            {
                SrednicaStudni = new Jednostka(Enums.Jednostki.Metr),
                PromienStudni = new Jednostka(Enums.Jednostki.Metr)
            };
            Aktualne = new UstawieniaJednostek();
        }

        public static Ustawienia StworzSingleton()
        {
            if (_instancja == null)
            {
                lock (Blokada)
                {
                    if (_instancja == null)
                    {
                        _instancja = new Ustawienia();
                    }
                }
            }

            return _instancja;
        }



    }
}

﻿using System.Collections.Generic;
using ValidationEngine.ValidationRules;

namespace ValidationEngine.Validators
{
    public class StringValidator : ValidatorBase
    {
        public StringValidator()
        {
            ValidationRules = new List<IValidationRule>
            {
                new StringNotNullOrEmptyValidationRule()
            };
        }
    }
}
